<%-- 
    Document   : leave_comment
    Created on : Feb 20, 2020, 11:53:22 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.LeaveComment"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String yearApply = AccountingPeriod.getCurYearByDate(AccountingPeriod.getCurrentTimeStamp());

    List<LeaveComment> listAll = (List<LeaveComment>) LeaveDAO.getAllLeaveComment(log, request.getParameter("leaveID"));
    int i = 0;

    if (!listAll.isEmpty()) {


%>
<div class="ibox">
    <div class="ibox-body">
        <p class="text-center mb-3">Ruangan Komen</p>
        <div class="cd-timeline cd-timeline-dark timeline-2">

            <%                for (LeaveComment j : listAll) {
                    i++;

                    CoStaff ls = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());
            %>
            <div class="cd-timeline-block">
                <div class="cd-timeline-icon bg-success text-white"><img class="img-circle" src="<%= ls.getImageURL()%>" alt="image" width="54" /></div>
                <div class="cd-timeline-content">
                    <div class="row">
                        <div class="col-lg-9 col-sm-12">
                            <p class="mb-0"><%= j.getComment()%></p>
                            <p class="mt-0"><small><%= GeneralTerm.capitalizeFirstLetter(ls.getName()) %></small></p>
                            <a class="text-blue askdetail" id="<%= request.getParameter("leaveID")%>" href="<%= j.getId() %>"><small><i class="fa fa-reply" aria-hidden="true"></i> Balas</small></a>
                        </div>
                        <div class="col-lg-3 col-sm-12">
                            <small><span class="cd-date need_to_be_rendered" datetime="<%= j.getDate() + " " + j.getTime()%>"></span></small>
                        </div>
                    </div>
                </div>
            </div>

            <%
                }

            %>
        </div>
        
    </div>
</div>
<%                }

%>
<script src="./assets/js/timeago.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        new timeago().render($('.need_to_be_rendered'));

    });
</script>