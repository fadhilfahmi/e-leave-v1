<%-- 
    Document   : leave_view_bymonth
    Created on : May 20, 2020, 12:18:47 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaffDepartment"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.BookingDestination"%>
<%@page import="com.lcsb.eleave.model.BookingMaster"%>
<%@page import="com.lcsb.eleave.dao.DateAndTime"%>
<%@page import="com.lcsb.eleave.model.Book"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>

<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    
        
   if (log != null) {
        

    String date = request.getParameter("date");
    if(date!=null){
        
    }else{
       
        String d = AccountingPeriod.getCurrentTimeStamp();
        
        date = d.substring(8, 10) +"-" + d.substring(5, 7) + "-" +  d.substring(0, 4);
        
    }
%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#addnew').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'conf_car_add.jsp');

            return false;
        });

        $('#change-date').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            $(location).attr('href', 'leave_view_bymonth.jsp?date=' + id);

            return false;
        });

        $('#change-view').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            $(location).attr('href', 'leave_calendar.jsp?filter=' + id);

            return false;
        });

        $('#viewcarmodal').click(function (e) {

            e.preventDefault();
            $.ajax({
                async: false,
                url: "PathController?process=addmodal",
                success: function (result) {
                    //alert(result);
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=viewbooking&id=" + id,
                success: function (result) {
                    $(location).attr('href', 'book_car_view.jsp?id=' + id);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('.deletebutton').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletebooking&bookID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'book_list.jsp');
                    }
                });

            });


            return false;
        });




        $('#date_1 .input-group.date').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'dd-mm-yyyy',
            orientation: "bottom"
        });



        /* var calendarEl = document.getElementById('calendar');
         
         var calendar = new FullCalendar.Calendar(calendarEl, {
         plugins: [ 'interaction', 'dayGrid' ],
         defaultDate: '2019-08-12',
         editable: true,
         eventLimit: true, // allow "more" link when too many events
         events: [
         {
         title: 'All Day Event',
         start: '2019-08-01'
         },
         {
         title: 'Long Event',
         start: '2019-08-07',
         end: '2019-08-10'
         },
         {
         groupId: 999,
         title: 'Repeating Event',
         start: '2019-08-09T16:00:00'
         },
         {
         groupId: 999,
         title: 'Repeating Event',
         start: '2019-08-16T16:00:00'
         },
         {
         title: 'Conference',
         start: '2019-08-11',
         end: '2019-08-13'
         },
         {
         title: 'Meeting',
         start: '2019-08-12T10:30:00',
         end: '2019-08-12T12:30:00'
         },
         {
         title: 'Lunch',
         start: '2019-08-12T12:00:00'
         },
         {
         title: 'Meeting',
         start: '2019-08-12T14:30:00'
         },
         {
         title: 'Happy Hour',
         start: '2019-08-12T17:30:00'
         },
         {
         title: 'Dinner',
         start: '2019-08-12T20:00:00'
         },
         {
         title: 'Birthday Party',
         start: '2019-08-13T07:00:00'
         },
         {
         title: 'Click for Google',
         url: 'http://google.com/',
         start: '2019-08-28'
         }
         ]
         });
         
         calendar.render();*/

        $('.activerowm').click(function (e) {
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });
        $('.activerowx').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_nd" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_nd" + b).toggleClass('warning');
        });
        $('.activerowy').click(function (e) {
            e.preventDefault();
            //alert(b);
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            //$(this).toggleClass('info');
            // $("#togglerow_st" + b).toggleClass('warning');
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>




        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ibox-fullheight">
                            <div class="ibox-head">
                                <div class="ibox-title">
                                    <span class="btn-icon-only btn-circle bg-primary-50 text-primary mr-3"><i class="ti-email"></i></span>Permohonan Cuti Pada <%= AccountingPeriod.fullDateMonth(date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2)) %></div>

                                <div class="ibox-tools">
                                    <div class="form-group" id="date_1">
                                        <label class="font-normal"></label>
                                        <div class="input-group date">
                                            <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control" id="change-date" type="text" value="<%= date%>">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="ibox-body">
                                <div class="flexbox mb-4">
                                    <div class="flexbox">

                                        <span class="flexbox mr-3">
                                            <span class="mr-2 text-muted">Lulus</span>
                                            <span class="h3 mb-0 text-success font-strong"><%= LeaveDAO.getLeaveCountByStatusAndDate(log, "Approved", date)%></span>
                                        </span>
                                        <span class="flexbox mr-3">
                                            <span class="mr-2 text-muted">Menunggu</span>
                                            <%

                                                int n = 0;

                                                n += LeaveDAO.getLeaveCountByStatusAndDate(log, "Checked", date);
                                                n += LeaveDAO.getLeaveCountByStatusAndDate(log, "Preparing", date);
                                                n += LeaveDAO.getLeaveCountByStatusAndDate(log, "Supported", date);

                                            %>
                                            <span class="h3 mb-0 text-warning font-strong"><%=n%></span>
                                        </span>
                                        <span class="flexbox mr-3">
                                            <span class="mr-2 text-muted">Semua</span>
                                            <span class="h3 mb-0 text-dark font-strong"><%= LeaveDAO.getLeaveCountByStatusAndDate(log, "Approved", date) + n%></span>
                                        </span> 
                                    </div>
                                    <!--<a class="flexbox" href="javascript:;" target="_blank">VIEW ALL<i class="ti-arrow-circle-right ml-2 font-18"></i></a>-->
                                </div>
                                <div class="ibox-fullwidth-block">
                                    <table class="table">
                                        <thead class="thead-default thead-lg">
                                            <tr>
                                                <th class="pl-4">Bahagian</th>
                                                <th>Lulus</th>
                                                <th>Menunggu</th>
                                                <th>Semua</th>
                                                <!--<th class="pr-4">Peratus</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                List<CoStaffDepartment> listAlldept = (List<CoStaffDepartment>) StaffDAO.getAllDepartmentWithoutGMMD(log);
                                                int l = 0;

                                                for (CoStaffDepartment k : listAlldept) {
                                                    l++;

                                                    int s = 0;
                                            %>
                                            <tr class="activerowy" id="<%=l%>">
                                                <td class="pl-4">
                                                    <div class="flexbox-b">
                                                        <!--<span class="btn-icon-only btn-primary font-20 mr-3">AC</span>-->
                                                        <div>
                                                            <h6 class="mb-1"><%= k.getDescp()%></h6>
                                                            <div>
                                                                <!--<span class="text-muted font-13"><i class="ti-calendar mr-2"></i>20.04.2018</span>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>

                                                    <%
                                                        int o = LeaveDAO.getLeaveCountForDepartmentByStatusAndDate(log, k.getId(), "Approved", date);

                                                        int m = 0;

                                                        m += LeaveDAO.getLeaveCountForDepartmentByStatusAndDate(log, k.getId(), "Checked", date);
                                                        m += LeaveDAO.getLeaveCountForDepartmentByStatusAndDate(log, k.getId(), "Preparing", date);
                                                        m += LeaveDAO.getLeaveCountForDepartmentByStatusAndDate(log, k.getId(), "Supported", date);
                                                        s = o + m;

                                                        int per1 = 0;
                                                        if (o > 0) {
                                                            per1 = o * 100 / s;
                                                        }
                                                    %>
                                                    <div class="easypie" data-percent="<%= per1%>" data-bar-color="green" data-size="56" data-line-width="3">
                                                        <span class="easypie-data h5 font-strong"><%= o%></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <%

                                                        int per = 0;
                                                        if (s > 0) {
                                                            per = m * 100 / s;
                                                        }

                                                    %>
                                                    <div class="easypie" data-percent="<%= per%>" data-bar-color="orange" data-size="56" data-line-width="3">
                                                        <span class="easypie-data h5 font-strong"><%= m%></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <%

                                                    %>
                                                    <h4 class="font-strong text-dark mb-0"><%=s%></h4>
                                                </td>
                                                <!--<td class="pr-4">
                                                    <div class="easypie" data-percent="59" data-bar-color="#5c6bc0" data-size="56" data-line-width="3">
                                                        <span class="easypie-data h5 font-strong">59%</span>
                                                    </div>
                                                </td>-->
                                            </tr>
                                            <tr id="togglerow_st<%= l%>" style="display: none">
                                                <td colspan="5">
                                                    <ul class="media-list media-list-divider ml-4 mr-2" >




                                                        <%
                                                            List<Leave> listdeptleave = (List<Leave>) LeaveDAO.getAllLeaveTodayByDept(log, date, k.getId());
                                                            int t = 0;

                                                            for (Leave r : listdeptleave) {
                                                                t++;

                                                                CoStaff st = (CoStaff) StaffDAO.getInfo(log, r.getStaffID());


                                                        %> <li class="media align-items-center">
                                                            <a class="media-img" href="javascript:;">
                                                                <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                                            </a>
                                                            <div class="media-body d-flex align-items-center">
                                                                <div class="flex-1">
                                                                    <div class="media-heading"><%= st.getName() %></div><small class="text-muted"><%= st.getPosition()%></small></div>
                                                                     <span class="badge badge-<%= LeaveDAO.getBadgeColor(r.getStatus())%> "><%= LeaveDAO.getMalayWord2(r.getStatus())%>
                                                            </div>
                                                        </li>



                                                        <%}%>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <%}%>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!--<div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">DRAGGABLE EVENTS</div>
                            </div>
                            <div class="ibox-body p-3">
                                <div id="external-events">
                                    <div class="ex-event" data-class="fc-event-primary"><i class="badge-point badge-primary mr-3"></i>Business</div>
                                    <div class="ex-event" data-class="fc-event-warning"><i class="badge-point badge-warning mr-3"></i>Reporting</div>
                                    <div class="ex-event" data-class="fc-event-success"><i class="badge-point badge-success mr-3"></i>Meeting</div>
                                    <div class="ex-event" data-class="fc-event-danger"><i class="badge-point badge-danger mr-3"></i>Important</div>
                                    <div class="ex-event" data-class="fc-event-info"><i class="badge-point badge-info mr-3"></i>Personal</div>
                                    <p class="ml-2 mt-4">
                                        <label class="checkbox checkbox-primary">
                                            <input id="drop-remove" type="checkbox">
                                            <span class="input-span"></span>remove after drop</label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-md-12">
                        <div class="ibox">

                            <div class="ibox-body">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- New Event Dialog-->
                <div class="modal fade" id="new-event-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <form class="modal-content form-horizontal" id="newEventForm" action="javascript:;">
                            <!--<div class="modal-header p-4">
                                <h5 class="modal-title">NEW EVENT</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>-->
                            <div class="modal-body p-4">
                                <div class="form-group mb-4">
                                    <label class="text-muted mb-3">Category</label>
                                    <div>
                                        <label class="radio radio-outline-primary radio-inline check-single" data-toggle="tooltip" data-original-title="General">
                                            <input type="radio" name="category" checked value="fc-event-primary">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-warning radio-inline check-single" data-toggle="tooltip" data-original-title="Payment">
                                            <input type="radio" name="category" value="fc-event-warning">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-success radio-inline check-single" data-toggle="tooltip" data-original-title="Technical">
                                            <input type="radio" name="category" value="fc-event-success">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-danger radio-inline check-single" data-toggle="tooltip" data-original-title="Registration">
                                            <input type="radio" name="category" value="fc-event-danger">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-info radio-inline check-single" data-toggle="tooltip" data-original-title="Security">
                                            <input type="radio" name="category" value="fc-event-info">
                                            <span class="input-span"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <input class="form-control form-control-line" id="new-event-title" type="text" name="title" placeholder="Title">
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">Start:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="new-event-start" type="text" name="start" value="">
                                        </div>
                                    </div>
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">End:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="new-event-end" type="text" name="end" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4 pt-3">
                                    <label class="ui-switch switch-icon mr-3 mb-0">
                                        <input id="new-event-allDay" type="checkbox" checked>
                                        <span></span>
                                    </label>All Day</div>
                            </div>
                            <div class="modal-footer justify-content-start bg-primary-50">
                                <button class="btn btn-primary btn-rounded" id="addEventButton" type="submit">Add event</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End New Event Dialog-->
                <!-- Event Detail Dialog-->
                <div id="modalhere">

                </div>
                <!-- End Event Detail Dialog-->
            </div>
            <!-- END PAGE CONTENT-->
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>

<script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

</html>
<%
 }else{
%>

 
<script type="text/javascript">
    window.location.href = "index.jsp";
</script>

<%
}
 
 
%>