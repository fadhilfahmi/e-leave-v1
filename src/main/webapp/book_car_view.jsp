<%-- 
    Document   : book_car_view
    Created on : Oct 10, 2019, 11:21:58 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.Car"%>
<%@page import="com.lcsb.smartbooking.dao.CarDAO"%>
<%@page import="com.lcsb.smartbooking.dao.MemberDAO"%>
<%@page import="com.lcsb.smartbooking.model.Members"%>
<%@page import="com.lcsb.smartbooking.model.BookingPassenger"%>
<%@page import="com.lcsb.smartbooking.model.BookingDestination"%>
<%@page import="com.lcsb.smartbooking.model.BookingMaster"%>
<%@page import="com.lcsb.smartbooking.model.BookingCar"%>
<%@page import="com.lcsb.smartbooking.model.Book"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Book b = (Book) BookingDAO.getBookingInfo(log, request.getParameter("id"));

    BookingMaster bm = (BookingMaster) BookingDAO.getBookingMaster(log, request.getParameter("id"));
    
    Book book = (Book) bm.getBooking();

    boolean isCarBooked = BookingDAO.isCarBooked(log, b.getBookID());
    
    String readyBtnDisabled= "disabled";
    String readyBtnString = "Ready to Confirm";
    
    if(BookingDAO.isBookingReadyToConfirm(log, request.getParameter("id")) == 0){
        readyBtnDisabled = "";
    }
    
    String btnConfirmClass = "btn-warning";
    if(book.getStatus().equals("Confirmed")){
        btnConfirmClass = "btn-success";
        readyBtnString = "Confirmed";
    }

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {





        $("#back").click(function () {
            parent.history.back();
            return false;
        });

        $('.assigncar').click(function (e) {
            e.preventDefault();

            var bookid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "PathController?process=assigncarmodal&bookID=" + bookid + "&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('.confirmbook').click(function (e) {
            e.preventDefault();

            var bookid = $(this).attr('id');
            $.ajax({
                async: false,
                url: "ProcessController?process=confirmbook&bookID=" + bookid,
                success: function (result) {
                    
                    if(result == 1){
                        toastr.success('Confirmed!');
                        $('#btnconfirm').html('<i class="fa fa-check"></i>Confirmed');
                        ('.confirmbook').addClass('btn-success').removeClass('btn-warning');
                        
                    }
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <button class="btn btn-primary btn-rounded  my-3" id="back">
                    <span class="btn-icon"><i class="fa fa-arrow-left"></i>Back To List</span>
                </button>
                <button class="btn <%=btnConfirmClass%> btn-rounded  my-3 confirmbook <%=readyBtnDisabled%>" id="<%= request.getParameter("id") %>">
                    <span class="btn-icon" id="btnconfirm"><i class="fa fa-check"></i><%=readyBtnString%></span>
                </button>
                <div class="d-flex align-items-center mb-5">
                    <span class="mr-4 static-badge badge-blue"><i class="ti-car"></i></span>
                    <div>
                        <h5 class="font-strong">Book ID #<%= b.getBookID()%></h5>
                        <div class="text-light"><%= b.getStaffname()%></div>
                    </div>
                </div>
                <div class="row">


                    <div class="col-sm-6">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Destination</div>
                            </div>
                            <div class="ibox-body">

                                <div class="cd-timeline cd-timeline-dark  timeline-2">
                                    <%List<BookingDestination> listAllDest = (List<BookingDestination>) bm.getListDestination();

                                        for (BookingDestination j : listAllDest) {

                                            BookingCar bc = (BookingCar) BookingDAO.getBookedCarByDestination(log, j.getId());
                                            Car car = (Car) CarDAO.getCarInfo(log, bc.getCarID());

                                            String drivetype = "Self Drive";

                                            if (bc.getDriverID() != null && !bc.getDriverID().equals("Self Drive")) {
                                                drivetype = "With Driver : " + DriverDAO.getDriverInfo(log, bc.getDriverID()).getDriverName();
                                            }

                                    %>
                                    <div class="cd-timeline-block">
                                        <div class="cd-timeline-icon bg-success text-white"><i class="fa fa-map-marker"></i></div>
                                        <div class="cd-timeline-content">
                                            <h4><%= j.getDesttype()%></h4>

                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <p><%= j.getDestdescp()%></p>
                                                    <small>From : <strong><%= DateAndTime.fullDateMonth(j.getStartdate())%></strong></small><br>
                                                    <small>To : <strong><%= DateAndTime.fullDateMonth(j.getEnddate())%></strong></small><br><br>




                                                </div>
                                                <div class="col-lg-6 col-sm-12">


                                                    <%
                                                        String assBtn = "Assign Driver & Car";
                                                        String assBtnColor = "warning";
                                                        if (bc.getDestID() != null) {
                                                            assBtn = "Assign Again";
                                                            assBtnColor = "success";
                                                    %>

                                                    <!--<small><i class="fa fa-car"></i> <strong><%= car.getDescp()%></strong></small><br>
                                                    <small><i class="fa fa-user"></i> <strong><%= drivetype%></strong></small><br><br>-->


                                                    <a class="card mr-5 mb-4" href="javascript:;" style="width: 260px;">
                                                        <div class="card-body flexbox pull-right">
                                                            <!--<img class="img-circle mr-3" src="./assets/img/users/u6.jpg" alt="image" width="48" />-->
                                                            <div class="text-right">
                                                                <h6 class="mb-1"><%= car.getDescp()%></h6><small class="text-muted"><%= drivetype%></small></div>
                                                        </div>
                                                    </a>
                                                    <%}%>
                                                    <a class="btn btn-<%=assBtnColor%> btn-sm assigncar pull-right" id="<%= bm.getBooking().getBookID()%>"
                                                       href="<%= j.getId()%>"><%= assBtn%></a>
                                                </div>
                                            </div>

                                            <span class="cd-date"><%= j.getDaycount()%> <%= BookingDAO.getDayWord(j.getDaycount())%></span>

                                        </div>
                                    </div>
                                    <%
                                        }
                                    %>
                                    <!--<div class="cd-timeline-block">
                                        <div class="cd-timeline-icon bg-primary text-white"><i class="fa fa-briefcase"></i></div>
                                        <div class="cd-timeline-content">
                                            <h4>Meeting</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                                            <a class="btn btn-success btn-sm"
                                               href="javascript:;">More ...</a>
                                            <span class="cd-date">2 Days ago</span>
                                        </div>
                                    </div>
                                    <div class="cd-timeline-block">
                                        <div class="cd-timeline-icon bg-warning text-white"><i class="fa fa-shopping-basket"></i></div>
                                        <div class="cd-timeline-content">
                                            <h4>Shopping with Olivia</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                                            <a class="btn btn-success btn-sm"
                                               href="javascript:;">More ...</a>
                                            <span class="cd-date">Dec 15</span>
                                        </div>
                                    </div>
                                    <div class="cd-timeline-block">
                                        <div class="cd-timeline-icon bg-blue text-white"><i class="fa fa-birthday-cake"></i></div>
                                        <div class="cd-timeline-content">
                                            <h4>Emma's birthday</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                                            <a class="btn btn-success btn-sm"
                                               href="javascript:;">More ...</a>
                                            <span class="cd-date">Dec 17</span>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="ibox ibox-fullheight">
                            <div class="ibox-head">
                                <div class="ibox-title">Passenger List</div>
                            </div>
                            <div class="ibox-body">
                                <ul class="media-list media-list-divider mr-2 scroller" data-height="480px">
                                    <%List<BookingPassenger> listAll = (List<BookingPassenger>) bm.getListPassenger();

                                        for (BookingPassenger j : listAll) {
                                            Members m = (Members) MemberDAO.getMemberInfo(log, j.getCompID());

                                    %>
                                    <li class="media align-items-center">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="<%= m.getImageURL()%>" alt="image" width="54" />
                                        </a>
                                        <div class="media-body d-flex align-items-center">
                                            <div class="flex-1">
                                                <div class="media-heading"><%= m.getMemberName()%></div><small class="text-muted"><%= m.getEmail()%></small></div>
                                            <!--<button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>-->
                                        </div>
                                    </li>
                                    <%                                        }
                                    %>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
</body>
</html>
