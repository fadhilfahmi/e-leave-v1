<%-- 
    Document   : view_leave_bygrid
    Created on : Jun 9, 2020, 11:00:36 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (log != null) {
        //String balLeave = "";
        double balLeave = 0;

        String curYear = AccountingPeriod.getCurYearByCurrentDate();
        String curMonth = AccountingPeriod.getCurPeriodByCurrentDate();
        String curDate = AccountingPeriod.getCurrentTimeStamp();

        if (LeaveDAO.isLeaveInfoExist(log, log.getUserID(), AccountingPeriod.getCurrentTimeStamp())) {
            balLeave = LeaveDAO.getBalanceEligibleLeave(log, log.getUserID(), AccountingPeriod.getCurYearByCurrentDate());
        }

        double bakiCutiLayak = LeaveDAO.getEligibleLeaveForTheMonth(log, log.getUserID(), curYear, curDate);
        double bakiMC = LeaveDAO.getTotalSickLeaveOfTheYear(log, log.getUserID(), curYear);


%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {



        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });

        $('.gotoleavetab').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavetab&tabid=' + id);

            return false;
        });

        $('.view-detail').click(function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'PathController?process=viewleavedetail&leaveID=' + leaveid);
            return false;
        });


        $('.view-modal').click(function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "PathController?process=viewleavemodal&leaveID=" + leaveid + "&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $(".actionto").unbind('click').bind('click', function (e) {

            $(this).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Tunggu ... ');
//var a = $("form").serialize();
            var id = $(this).attr('id');
            var action = $(this).attr('title');

            var stringnoti = 'disahkan';

            if (action == 'Rejected') {
                stringnoti = 'dibatalkan';
            } else if (action == 'Approved') {
                stringnoti = 'diluluskan';
            } else if (action == 'Supported') {
                stringnoti = 'disokong';
            }


            $.ajax({
                async: true,
                url: "PathController?process=validateleave&leaveID=" + id + "&action=" + action,
                success: function (result) {
                    //if (result > 0) {

                    //}

                    if (action == 'Rejected') {
                        alertify
                                .defaultValue("")
                                .prompt("Nyatakan sebab anda tolak permohonan ini.",
                                        function (val, ev) {
                                            ev.preventDefault();
                                            $.ajax({
                                                async: true,
                                                url: "PathController?process=leavecomment&leaveID=" + id + "&comment=" + val,
                                                success: function (result) {
                                                    //if (result > 0) {

                                                    //}


                                                    alertify.success("Mesej anda telah disimpan.");
                                                    //$(location).attr('href', 'leave_list_approval.jsp?tab=2');
                                                    $.ajax({
                                                        url: "leave_comment.jsp?leaveID=" + id,
                                                        success: function (result) {
                                                            $('#comment-section').empty().html(result).hide().fadeIn(300);
                                                            swal({
                                                                title: "Cuti telah " + stringnoti,
                                                                text: "Sila semak e-mail untuk maklumat lanjut",
                                                                type: "success"
                                                            }, function () {
                                                                $(location).attr('href', 'PathController?process=viewleavegrid&leaveID=' + id);
                                                            });
                                                        }
                                                    });


                                                }
                                            });


                                        }, function (ev) {
                                    ev.preventDefault();
                                    //alertify.error("You've clicked Cancel");
                                }
                                );
                    } else {
                        swal({
                            title: "Cuti telah " + stringnoti,
                            text: "Sila semak e-mail untuk maklumat lanjut",
                            type: "success"
                        }, function () {
                            $(location).attr('href', 'PathController?process=viewleavegrid&leaveID=' + id);
                        });

                    }

                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


    });

</script>
<style>
    .label {
        position:absolute;
        left: 0;
        top: 0
    }
</style>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <!-- <div class="page-heading">
                 <h5 class="page-title">Cuti yang perlu tindakan.</h5>
            <!--<ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html"><i class="la la-home font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Basic UI</li>
                <li class="breadcrumb-item">badges &amp; Progress</li>
            </ol>
        </div>-->
            <div class="page-content fade-in-up">
                <%                    List<Leave> listAllStaffbyGrid = (List<Leave>) LeaveDAO.getAllLeaveExcludeYouGridByStaff(log);
                    int s = 0;

                    if (listAllStaffbyGrid.isEmpty()) {

                %>


                <div class="alert alert-success alert-dismissable fade show has-icon"><i class="la la-warning alert-icon"></i>
                    <strong>Terima kasih!</strong><br>Tiada permohonan cuti baru. Semua cuti sudah diambil tindakan. <br><a class="btn btn-light btn-sm mt-3" href="Login">Kembali ke Halaman utama</a></div>

                <%                } else {%>

                <div class="alert alert-warning alert-dismissable fade show has-icon"><i class="la la-warning alert-icon"></i>
                    <strong>Perhatian!</strong><br>Anda masih mempunyai cuti-cuti yang perlu diambil tindakan seperti dibawah. </div>


                <%
                    }%>
                <div class="row">

                    <%     for (Leave r : listAllStaffbyGrid) {

                            CoStaff co = (CoStaff) StaffDAO.getInfo(log, r.getStaffID());

                            String buttonValid = "";
                            String titleValid = "";

                            if (log.getAccessLevel() == 2) {

                                buttonValid = "Sokong";
                                titleValid = "Supported";
                            } else if (log.getAccessLevel() == 1) {

                                buttonValid = "Semak";
                                titleValid = "Checked";
                            } else if (log.getAccessLevel() == 3) {

                                if (LeaveDAO.getLevelThreeSupportOrApprove(log, r.getStaffID()).equals("support")) {
                                    buttonValid = "Sokong";
                                    titleValid = "Supported";
                                } else if (LeaveDAO.getLevelThreeSupportOrApprove(log, r.getStaffID()).equals("approve")) {
                                    buttonValid = "Lulus";
                                    titleValid = "Approved";
                                }

                            } else if (log.getAccessLevel() == 4) {

                                buttonValid = "Sah";
                                titleValid = "Verified";
                            }
                    %>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card overflow-visible mt-5">
                            <span class="label label-important badge badge-<%= LeaveDAO.getBadgeColor(r.getStatus())%> "><%= LeaveDAO.getMalayWord2(r.getStatus())%></span>
                            <div class="card-body text-center mt-4">
                                <img class="img-circle img-bordered card-abs-top-center" src="<%= co.getImageURL()%>" alt="image" width="60" />
                                <h6 class="mb-1">
                                    <a><%= co.getName()%></a>
                                </h6><small><span class="badge badge-secondary mr-2"><%= r.getDays()%> hari</span></small><small class="text-muted"><span class="badge-<%= LeaveDAO.getBadgeColorTypeLeave(r.getType())%> badge-point"></span> <%= r.getType()%></small>
                                <div class="d-flex justify-content-around align-items-center mt-2">
                                    <p> <small><%= AccountingPeriod.fullDateMonth(r.getDatestart())%> - <%= AccountingPeriod.fullDateMonth(r.getDateend())%><span class="badge-secondary badge-point mr-2 ml-2"></span> <%= r.getReason()%></small></p>
                                </div>
                                <div class="d-flex justify-content-around align-items-center mt-2 font-16">
                                    <button class="btn btn-outline-blue btn-sm btn-fix view-detail mr-2 ml-2" title="Perincian Cuti" id="<%=r.getLeaveID()%>"">Perincian</button>
                                    <button class="btn btn-outline-success btn-sm btn-fix actionto mr-2" title="<%= titleValid%>" id="<%=r.getLeaveID()%>"><%= buttonValid%></button>
                                    <button class="btn btn-outline-danger btn-sm btn-fix actionto mr-2" title="Rejected" id="<%=r.getLeaveID()%>"">Tolak</button>
                                    <!--<a class="text-primary" href="javascript:;"><i class="fa fa-facebook"></i></a>
                                    <a class="text-primary" href="javascript:;"><i class="fa fa-twitter"></i></a>
                                    <a class="text-primary" href="javascript:;"><i class="fa fa-pinterest-p"></i></a>
                                    <a class="text-primary" href="javascript:;"><i class="fa fa-instagram"></i></a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <%}%>
                </div>
                <!-- END PAGE CONTENT-->
                <jsp:include page='layout/footer.jsp'>
                    <jsp:param name="page" value="home"/>
                </jsp:include>
            </div>
        </div>

        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                var data1 = [];
                var data3 = [];

                $.ajax({
                    async: false,
                    url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Tahunan",
                    success: function (result) {
                        // $("#haha").html(result);
                        //console.log(result);
                        //data1 = result;
                        //console.log(data1);
                        var obj = jQuery.parseJSON(result);
                        var i = 0;
                        // console.log(obj);
                        $.each(obj, function (key, value) {
                            i++;
                            console.log(key);
                            data1.push(value);
                            //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                        });
                        console.log(data1);

                    }});



                $.ajax({
                    async: false,
                    url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Sakit",
                    success: function (result) {
                        // $("#haha").html(result);
                        //console.log(result);
                        //data1 = result;
                        //console.log(data1);
                        var obj = jQuery.parseJSON(result);
                        var i = 0;
                        // console.log(obj);
                        $.each(obj, function (key, value) {
                            i++;
                            console.log(key);
                            data3.push(value);
                            //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                        });
                        console.log(data3);

                    }});

                var ctx = document.getElementById("visitors_chart").getContext("2d");
                var visitors_chart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                        datasets: [
                            {
                                label: "Sakit",
                                data: data3,
                                borderColor: 'rgba(117,54,230,0.9)',
                                backgroundColor: 'rgba(117,54,230,0.9)',
                                pointBackgroundColor: 'rgba(117,54,230,0.9)',
                                pointBorderColor: 'rgba(117,54,230,0.9)',
                                borderWidth: 1,
                                pointBorderWidth: 1,
                                pointRadius: 0,
                                pointHitRadius: 30,
                            }, {
                                label: "Tahunan",
                                data: data1,
                                borderColor: 'rgba(104,218,221,1)',
                                backgroundColor: 'rgba(104,218,221,1)',
                                pointBackgroundColor: 'rgba(104,218,221,1)',
                                pointBorderColor: 'rgba(104,218,221,1)',
                                borderWidth: 1,
                                pointBorderWidth: 1,
                                pointRadius: 0,
                                pointHitRadius: 30,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        showScale: false,
                        scales: {
                            xAxes: [{
                                    gridLines: {
                                        display: false,
                                    },
                                }],
                            yAxes: [{
                                    gridLines: {
                                        display: false,
                                        drawBorder: false,
                                    },
                                }]
                        },
                        legend: {
                            labels: {
                                boxWidth: 12
                            }
                        },
                    }
                });

                visitors_data = {

                    1: {
                        data: [
                            data3,
                            data1,
                        ],
                        labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                    },
                    2: {
                        data: [
                            [2, 1, 4, 7],
                            [6, 2, 1, 3],
                            [3, 1, 2, 5],
                        ],
                        labels: ["Minggu 1", "Minggu 2", "Minggu 3", "Minggu 4"],
                    },
                };

                $('#chart_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var id = $(this).attr('data-id');
                    if (id && +id in visitors_data) {
                        visitors_chart.data.labels = visitors_data[id].labels;
                        var datasets = visitors_chart.data.datasets;
                        $.each(datasets, function (index, value) {
                            datasets[index].data = visitors_data[id].data[index];
                        });
                    }
                    visitors_chart.update();
                });
            });
        </script>
</body>
</html>

<%
} else {
%>


<script type="text/javascript">
    window.location.href = "index.jsp";
</script>

<%
    }


%>

