<%-- 
    Document   : book_car_view
    Created on : Oct 10, 2019, 11:21:58 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.BookingMaster"%>
<%@page import="com.lcsb.smartbooking.model.BookingCar"%>
<%@page import="com.lcsb.smartbooking.model.Book"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Book b = (Book) BookingDAO.getBookingInfo(log, request.getParameter("id"));
    
    BookingMaster bm = (BookingMaster) BookingDAO.getBookingMaster(log, request.getParameter("id"));
    
    boolean isCarBooked = BookingDAO.isCarBooked(log, b.getBookID());

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $("#savebutton").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();

            var a = $("#saveform :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=savebook",
                success: function (result) {
                    $(location).attr('href', 'book_list.jsp');
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


        $("#back").click(function () {
            parent.history.back();
            return false;
        });

        $('.assigncar').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=assigncarmodal&bookID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="d-flex align-items-center mb-5">
                    <span class="mr-4 static-badge badge-blue"><i class="ti-car"></i></span>
                    <div>
                        <h5 class="font-strong">Book ID #<%= b.getBookID()%></h5>
                        <div class="text-light"><%= b.getStaffname()%></div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-xl-4">
                        <div class="ibox">
                            <div class="ibox-body">
                                <h5 class="font-strong mb-4">Booking Information</h5>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Status</div>
                                    <div class="col-8">
                                        <span class="badge badge-<%= BookingDAO.getBadgeColor(b.getStatus())%> badge-pill"><%= b.getStatus()%></span>
                                    </div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Destination</div>
                                    <div class="col-8"><%= b.getDestination()%></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">From</div>
                                    <div class="col-8"><%= DateAndTime.fullDateMonth(b.getStartdate())%></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">To</div>
                                    <div class="col-8"><%= DateAndTime.fullDateMonth(b.getEnddate())%></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Total Day(s)</div>
                                    <div class="col-8"><%= b.getDaycount()%> <%= BookingDAO.getDayWord(b.getDaycount())%></div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-4 text-light">Reason</div>
                                    <div class="col-8"><%= b.getReason()%></div>
                                </div>
                            </div>
                        </div>
                        <div class="ibox">
                            <div class="ibox-body">
                                <h5 class="font-strong mb-4">Person</h5>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Name</div>
                                    <div class="col-8"><%= b.getStaffID()%> <%= b.getStaffname()%></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Email</div>
                                    <div class="col-8"><%= b.getEmail()%></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Position</div>
                                    <div class="col-8">To be filled</div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-4 text-light">Phone</div>
                                    <div class="col-8">+380681478544</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="row align-top">
                                     <div class="col-8"><h5 class="font-strong mb-4">Car Assigned</h5></div>
                                     
                                     <% if(isCarBooked){ %>
                                      <div class="col-4"><a id="<%= b.getBookID() %>" class="btn btn-rounded btn-primary btn-air assigncar" href="ecommerce_add_product.html">Assign Again</a></div>
                                      <% }else{ %>
                                      <div class="col-4"><a  id="<%= b.getBookID() %>"class="btn btn-rounded btn-primary btn-air assigncar" href="#">Assign Car</a></div>
                                      <% } %>
                                </div>
                                
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Status</div>
                                    <div class="col-8">
                                        <%= BookingDAO.getWordBookedCar(isCarBooked) %>
                                    </div>
                                </div>
                                    <%
                                    if(isCarBooked){
                                        
                                        BookingCar bc = (BookingCar) BookingDAO.getBookedCar(log, b.getBookID());
                                    
                                    %>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Drive Type</div>
                                    <div class="col-8"><%= bc.getDrivetype() %></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">From</div>
                                    <div class="col-8"><%= DateAndTime.fullDateMonth(b.getStartdate())%></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">To</div>
                                    <div class="col-8"><%= DateAndTime.fullDateMonth(b.getEnddate())%></div>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-4 text-light">Total Day(s)</div>
                                    <div class="col-8"><%= b.getDaycount()%> <%= BookingDAO.getDayWord(b.getDaycount())%></div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-4 text-light">Reason</div>
                                    <div class="col-8"><%= b.getReason()%></div>
                                </div>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
</body>
</html>
