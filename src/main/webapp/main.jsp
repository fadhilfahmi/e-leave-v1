<%-- 
    Document   : main
    Created on : Sep 29, 2019, 9:59:49 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //String balLeave = "";
    double balLeave = 0;

    String curYear = AccountingPeriod.getCurYearByCurrentDate();
    String curMonth = AccountingPeriod.getCurPeriodByCurrentDate();
    String curDate = AccountingPeriod.getCurrentTimeStamp();

    double percentYear = LeaveDAO.getPercentLeaveUseYear(log, log.getUserID(), curYear);
    double percentMonth = LeaveDAO.getPercentLeaveUseMonth(log, log.getUserID(), curYear, curMonth, AccountingPeriod.getCurrentTimeStamp());
    double percentYearMedical = LeaveDAO.getPercentMedicalLeaveUseYear(log, log.getUserID(), curYear);

    if (LeaveDAO.isLeaveInfoExist(log, log.getUserID(), AccountingPeriod.getCurrentTimeStamp())) {
        balLeave = LeaveDAO.getBalanceEligibleLeave(log, log.getUserID(), AccountingPeriod.getCurYearByCurrentDate());
    }

    double bakiCutiLayak = LeaveDAO.getEligibleLeaveForTheMonth(log, log.getUserID(), curYear, curDate);
    double bakiMC = LeaveDAO.getTotalSickLeaveOfTheYear(log, log.getUserID(), curYear);
%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {



        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });

        $('.gotoleavetab').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavetab&tabid=' + id);

            return false;
        });


        $('.view-modal').click(function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "PathController?process=viewleavemodal&leaveID=" + leaveid + "&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">

                <div class="row mb-4">
                    <div class="col-sm-12 col-lg-12">

                        <%
                            CoStaff co = (CoStaff) StaffDAO.getInfo(log, log.getUserID());
                            if (co.getPosition().equalsIgnoreCase("PENASIHAT KANAN PERLADANGAN") || co.getPosition().equalsIgnoreCase("PENASIHAT PERKILANGAN") || co.getPosition().equalsIgnoreCase("PENASIHAT KANAN PERKILANGAN") || co.getPosition().equalsIgnoreCase("PENASIHAT PERLADANGAN") || co.getPosition().equalsIgnoreCase("EKSEKUTIF KANAN") || co.getPosition().equalsIgnoreCase("PENGURUS BAHAGIAN") || co.getPosition().equalsIgnoreCase("AGRONOMIS") || co.getPosition().equalsIgnoreCase("TIMBALAN PENGURUS BESAR")) {
                        %>

                        <div class="alert alert-danger alert-dismissable fade show has-icon"><i class="la la-warning alert-icon"></i>
                            <strong>Makluman!</strong><br>Hirarki cuti anda telah berubah bermula 1 Oktober 2020. <br><a class="btn btn-danger btn-fix btn-sm mt-3" href="hirarki.jsp">Lihat hirarki terkini</a></div>

                        <%
                            }
                        %>

                        <div class="card overflow-visible mt-5 bg-primary" >
                            <div class="card-body text-center mt-4">
                                <img class="img-circle img-bordered card-abs-top-center" src="<%= log.getImageURL()%>" alt="image" width="60" />
                                <h6 class="mb-1">
                                    <a class="text-white"><%= log.getFullname()%></a>
                                </h6><small class="text-white"><%= StaffDAO.getInfo(log, log.getUserID()).getPosition()%></small><br>
                                <small class="text-white"><%

                                    String dept = StaffDAO.getInfo(log, log.getUserID()).getDepartment();
                                    if (!dept.equals("TIADA")) {
                                        out.println(dept);
                                    }


                                    %></small>
                                    <%                                    int leavePrep = LeaveDAO.getLeaveInProcess(log);
                                        if (leavePrep > 0) {
                                    %>
                                <div class="d-flex justify-content-around align-items-center mt-3 font-16">
                                    <a href="leave_list_approval.jsp?tab=1"><small class="text-white"><span class="badge badge-danger badge-circle"><%=leavePrep%></span> Cuti sedang diproses.</small></a>
                                    <a class="text-white" href="javascript:;"><small><i class="fa fa-calendar"></i> <%= AccountingPeriod.getFullCurrentDate()%></small></a>

                                </div>
                                <%
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-lg-4 col-md-6">
                        <div class="alert alert-warning alert-dismissable fade show alert-bordered has-icon"><i class="la la-warning alert-icon"></i>
                            <strong>Makluman</strong><br><span class="badge badge-danger badge-circle"><%=leavePrep%></span> cuti sedang diproses. <a href="leave_list_approval.jsp?tab=1"><u>Lihat</u></a></div>

                    </div>-->
                    <!--<div class="col-lg-4 col-md-6">
                        <div class="alert alert-warning alert-dismissable fade show alert-bordered has-icon"><i class="la la-warning alert-icon"></i>
                            <strong>Makluman</strong><br>Anda mempunyai <span class="badge badge-danger badge-circle">1</span> cuti yang sedang diproses. </div>
                    </div>-->
                </div>
                <%
                    // }
%>

                <div class="row mb-4">
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="card bg-success">
                            <div class="card-body">
                                <h2 class="text-white"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getBalanceEligibleLeave(log, co.getStaffid(), curYear))%> <i class="fa fa-briefcase float-right" aria-hidden="true"></i></h2>
                                <div><span class="text-white">CUTI TAHUNAN</span></div>
                                <div class="text-white mt-1">
                                    <small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveWithoutCF(log, co.getStaffid(), curYear))%></strong></span> Layak</small>
                                    <small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getPreviousLeaveCF(log, co.getStaffid(), curYear))%></strong></span> Dibawa</small>
                                    <small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveOfTheYear(log, co.getStaffid(), curYear))%></strong></span> Jumlah</small>
                                </div>
                            </div>
                            <div class="progress mb-2 widget-dark-progress">
                                <div class="progress-bar" role="progressbar" style="width:<%= percentYear%>%; height:5px;" aria-valuenow="<%= percentYear%>" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="card bg-primary">
                            <div class="card-body">
                                <h2 class="text-white"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(bakiCutiLayak)%> <i class="fa fa-calendar-check-o float-right" aria-hidden="true"></i></i></h2>
                                <div><span class="text-white">BAKI LAYAK BULAN</span></div>
                                <div class="text-white mt-1"><small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveForTheMonthExcludeApprovedThisMonth(log, co.getStaffid(), curYear, AccountingPeriod.getCurrentTimeStamp()))%></strong></span> hari jumlah layak bulan ini.</small></div>
                            </div>
                            <div class="progress mb-2 widget-dark-progress">
                                <div class="progress-bar" role="progressbar" style="width:<%=percentMonth%>%; height:5px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="card bg-danger">
                            <div class="card-body">
                                <h2 class="text-white"><%= LeaveDAO.getBalanceTotalSickLeaveOfTheYear(log, co.getStaffid(), curYear)%> <i class="fa fa-medkit float-right" aria-hidden="true"></i></h2>
                                <div><span class="text-white">CUTI SAKIT</span></div>
                                <div class="text-white mt-1">
                                    <span class="badge mr-1 widget-dark-badge"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getMedicalLeaveOfTheYear(log, co.getStaffid(), curYear))%></span><small>hari baki Cuti Sakit.</small></div>
                            </div>
                            <div class="progress mb-2 widget-dark-progress">
                                <div class="progress-bar" role="progressbar" style="width:<%=percentYearMedical%>%; height:5px;" aria-valuenow="<%=percentYearMedical%>" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>


                    <%
                        if (log.getAccessLevel() == 2 || log.getAccessLevel() == 3) {
                    %>
                    <div class="col-lg-4 col-md-6">
                        <div class="card mb-4">
                            <div class="card-body flexbox-b gotoleavetab" id="2">
                                <div class="easypie mr-4" data-percent="70" data-bar-color="#ff4081" data-size="80" data-line-width="8">
                                    <span class="easypie-data text-pink" style="font-size:32px;"><i class="la la-tags"></i></span>
                                </div>
                                <div>
                                    <h3 class="font-strong text-pink"><%= LeaveDAO.getLeaveCountForSupervisee(log)%></h3>
                                    <div class="text-muted">Cuti Seliaan</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%}%>
                </div>

                <div class="row mb-4">
                    <div class="col-lg-4">
                        <div class="ibox">
                            <%
                                List<Leave> listAlls = (List<Leave>) LeaveDAO.getAllConfirmedLeaveToday(log, "none");
                                List<Leave> listAllPending = (List<Leave>) LeaveDAO.getAllPendingLeaveToday(log, "none");
                            %>
                            
                            <!--<div class="ibox-title">Cuti Hari Ini </div>
                                <div class="ibox-tools">
                                    <a href="leave_calendar.jsp?filter=<%= StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()%>"><span class="badge badge-blue">Kalendar</span></a>
                                    
                                </div>-->

                            <div class="ibox-head flex-row-reverse">
                                <div class="ibox-title">
                                    <small>Hari Ini</small> <a href="leave_calendar.jsp?filter=<%= StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()%>"><span class="badge badge-blue">Kalendar</span></a></div>
                                <ul class="nav nav-tabs tabs-line">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#tab-2-1" data-toggle="tab"><i class="fa fa-check-circle-o mr-1" aria-hidden="true"></i><span class="button-text-approve">  Lulus</span><span class="badge badge-success badge-circle ml-1"><%= (listAlls.size() == 0) ? "" : listAlls.size()%></span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab-2-2" data-toggle="tab"><i class="fa fa-clock-o mr-1" aria-hidden="true"></i><span class="button-text-approve">Menunggu</span><span class="badge badge-warning badge-circle ml-1"><%= (listAllPending.size() == 0) ? "" : listAllPending.size()%></span></a>
                                    </li>
                                    <!--<li class="nav-item">
                                        <a class="nav-link" href="#tab-2-3" data-toggle="tab">Third</a>
                                    </li>-->
                                </ul>
                            </div>
                            <div class="ibox-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active " id="tab-2-1">
                                        <ul class="media-list media-list-divider mr-2"><!--data-height="580px"-->

                                            <%

                                                int m = 0;

                                                if (listAlls.isEmpty()) {

                                            %>
                                            <li class="media align-items-center">
                                                Tiada rekod

                                            </li>
                                            <%                                            }

                                                for (Leave j : listAlls) {
                                                    m++;

                                                    CoStaff st = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());


                                            %>
                                            <li class="media align-items-center">
                                                <a class="media-img" href="javascript:;">
                                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                                </a>
                                                <div class="media-body d-flex align-items-center">
                                                    <div class="flex-1">
                                                        <div class="media-heading"><%= st.getName()%></div><span class="badge-<%= LeaveDAO.getBadgeColorTypeLeave(j.getType())%> badge-point"></span> <small class="text-muted">
                                                            <%
                                                                if (j.getDays() > 1) {
                                                                    out.println(AccountingPeriod.fullDateMonth(j.getDatestart()) + " - " + AccountingPeriod.fullDateMonth(j.getDateend()));
                                                                } else {
                                                                    out.println(AccountingPeriod.fullDateMonth(j.getDatestart()));
                                                                }
                                                            %>


                                                        </small></div>

                                                </div>
                                            </li>

                                            <%
                                                }
                                            %>

                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="tab-2-2">
                                        <ul class="media-list media-list-divider mr-2"><!--data-height="580px"-->

                                            <%
                                                int y = 0;

                                                if (listAllPending.isEmpty()) {

                                            %>
                                            <li class="media align-items-center">
                                                Tiada rekod

                                            </li>
                                            <%                                            }

                                                for (Leave j : listAllPending) {
                                                    y++;

                                                    CoStaff st = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());


                                            %>
                                            <li class="media align-items-center">
                                                <a class="media-img" href="javascript:;">
                                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                                </a>
                                                <div class="media-body d-flex align-items-center">
                                                    <div class="flex-1">
                                                        <div class="media-heading"><%= st.getName()%></div><span class="badge-<%= LeaveDAO.getBadgeColorTypeLeave(j.getType())%> badge-point"></span> <small class="text-muted">
                                                            <%
                                                                if (j.getDays() > 1) {
                                                                    out.println(AccountingPeriod.fullDateMonth(j.getDatestart()) + " - " + AccountingPeriod.fullDateMonth(j.getDateend()));
                                                                } else {
                                                                    out.println(AccountingPeriod.fullDateMonth(j.getDatestart()));
                                                                }
                                                            %>


                                                        </small></div>

                                                </div>
                                            </li>

                                            <%
                                                }
                                            %>

                                        </ul>
                                    </div>
                                    <!--<div class="tab-pane fade text-center" id="tab-2-3">
                                        <div class="h1 mt-5 mb-3">TAB 3</div>
                                        <p class="mb-5">Laborum dolore consectetur occaecat in ex sunt et dolore dolore aliqua aute qui cupidatat et ullamco in pariatur dolor quis laboris ut.</p>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="d-flex justify-content-between mb-4">
                                    <div>
                                        <h3 class="m-0">Analisis Cuti</h3>
                                        <div>Analisa cuti anda pada tahun dan bulan</div>
                                    </div>
                                    <ul class="nav nav-pills nav-pills-rounded nav-pills-air" id="chart_tabs">
                                        <li class="nav-item ml-1">
                                            <a class="nav-link active" data-toggle="tab" data-id="1" href="javascript:;">Tahun</a>
                                        </li>
                                        <li class="nav-item ml-1">
                                            <a class="nav-link" data-toggle="tab" data-id="2" href="javascript:;">Bulan</a>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <canvas id="visitors_chart" style="height:260px;"></canvas>
                                </div>
                            </div>
                            <!--<hr>
                            <div class="ibox-body">
                                <div class="row">
                                    <div class="col-6 pl-4">
                                        <h6 class="mb-3">GENDER</h6>
                                        <span class="h2 mr-3"><i class="fa fa-male text-primary h1 mb-0 mr-2"></i>
                                            <span>56<sup>%</sup></span>
                                        </span>
                                        <span class="h2 mr-3"><i class="fa fa-female text-pink h1 mb-0 mr-2"></i>
                                            <span>32<sup>%</sup></span>
                                        </span>
                                        <span class="h2"><i class="fa fa-question text-light h1 mb-0 mr-2"></i>
                                            <span>12<sup>%</sup></span>
                                        </span>
                                    </div>
                                    <div class="col-6">
                                        <h6 class="mb-3">SCREENS</h6>
                                        <span class="h2 mr-3"><i class="ti-desktop text-primary mr-2"></i>
                                            <span>49<sup>%</sup></span>
                                        </span>
                                        <span class="h2 mr-3"><i class="ti-tablet text-pink mr-2"></i>
                                            <span>29<sup>%</sup></span>
                                        </span>
                                        <span class="h2"><i class="ti-mobile text-success mr-2"></i>
                                            <span>22<sup>%</sup></span>
                                        </span>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>

                <div id="modalhere"></div>
            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            var data1 = [];
            var data3 = [];

            $.ajax({
                async: false,
                url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Tahunan",
                success: function (result) {
                    // $("#haha").html(result);
                    //console.log(result);
                    //data1 = result;
                    //console.log(data1);
                    var obj = jQuery.parseJSON(result);
                    var i = 0;
                    // console.log(obj);
                    $.each(obj, function (key, value) {
                        i++;
                        console.log(key);
                        data1.push(value);
                        //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                    });
                    console.log(data1);

                }});



            $.ajax({
                async: false,
                url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Sakit",
                success: function (result) {
                    // $("#haha").html(result);
                    //console.log(result);
                    //data1 = result;
                    //console.log(data1);
                    var obj = jQuery.parseJSON(result);
                    var i = 0;
                    // console.log(obj);
                    $.each(obj, function (key, value) {
                        i++;
                        console.log(key);
                        data3.push(value);
                        //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                    });
                    console.log(data3);

                }});

            var ctx = document.getElementById("visitors_chart").getContext("2d");
            var visitors_chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                    datasets: [
                        {
                            label: "Sakit",
                            data: data3,
                            borderColor: 'rgba(117,54,230,0.9)',
                            backgroundColor: 'rgba(117,54,230,0.9)',
                            pointBackgroundColor: 'rgba(117,54,230,0.9)',
                            pointBorderColor: 'rgba(117,54,230,0.9)',
                            borderWidth: 1,
                            pointBorderWidth: 1,
                            pointRadius: 0,
                            pointHitRadius: 30,
                        }, {
                            label: "Tahunan",
                            data: data1,
                            borderColor: 'rgba(104,218,221,1)',
                            backgroundColor: 'rgba(104,218,221,1)',
                            pointBackgroundColor: 'rgba(104,218,221,1)',
                            pointBorderColor: 'rgba(104,218,221,1)',
                            borderWidth: 1,
                            pointBorderWidth: 1,
                            pointRadius: 0,
                            pointHitRadius: 30,
                        },
                    ],
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    showScale: false,
                    scales: {
                        xAxes: [{
                                gridLines: {
                                    display: false,
                                },
                            }],
                        yAxes: [{
                                gridLines: {
                                    display: false,
                                    drawBorder: false,
                                },
                            }]
                    },
                    legend: {
                        labels: {
                            boxWidth: 12
                        }
                    },
                }
            });

            visitors_data = {

                1: {
                    data: [
                        data3,
                        data1,
                    ],
                    labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                },
                2: {
                    data: [
                        [2, 1, 4, 7],
                        [6, 2, 1, 3],
                        [3, 1, 2, 5],
                    ],
                    labels: ["Minggu 1", "Minggu 2", "Minggu 3", "Minggu 4"],
                },
            };

            $('#chart_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var id = $(this).attr('data-id');
                if (id && +id in visitors_data) {
                    visitors_chart.data.labels = visitors_data[id].labels;
                    var datasets = visitors_chart.data.datasets;
                    $.each(datasets, function (index, value) {
                        datasets[index].data = visitors_data[id].data[index];
                    });
                }
                visitors_chart.update();
            });
        });
    </script>
</body>
</html>
