<%-- 
    Document   : list_destination
    Created on : Oct 29, 2019, 10:54:55 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.dao.EstateDAO"%>
<%@page import="com.lcsb.smartbooking.model.EstateInfo"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

%>
<script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />

<div class="form-group mb-4">
    <label class="font-normal"><%=request.getParameter("type")%></label>

    <%        if (!request.getParameter("type").equals("Others")) {
    %>
    <select class="show-tick form-control" title="Please select" data-style="btn-solid" name="destination2" id="selectdestination">
        <option value="None">Select</option>
        <%List<EstateInfo> listAll = (List<EstateInfo>) EstateDAO.getAllEstate(log, null, request.getParameter("type"));

            for (EstateInfo j : listAll) {

        %>
        <option value="<%= j.getEstatecode()%>" data-descp="<%= j.getEstatedescp()%>" data-type="<%= request.getParameter("type")%>"><%= j.getEstatedescp()%></option>

        <%}%>
    </select>

    <%
    } else {
    %>
    <div class="input-group">
        <input class="form-control" type="text" name="otherdestination" id="otherdestination" placeholder="Enter your destination here">
        <span class="input-group-btn">
            <button class="btn btn-outline-secondary" id="enterdestination">Enter</button>
        </span>
    </div>
    <%
        }
    %>
</div>

<script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>

