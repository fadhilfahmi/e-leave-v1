<%-- 
    Document   : test
    Created on : May 21, 2020, 5:16:29 AM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<ul class="media-list media-list-divider mr-2" data-height="470px">
                <!--<li class="media align-items-center">
                    <a class="media-img" href="javascript:;">
                        <img class="img-circle" src="./assets/img/users/u8.jpg" alt="image" width="54" />
                    </a>
                    <div class="media-body d-flex align-items-center">
                        <div class="flex-1">
                            <div class="media-heading">Lynn Weaver</div><small class="text-muted">Lorem Ipsum is simply dummy</small></div>
                        <button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>
                    </div>
                </li>-->
                <%List<Leave> listAll = (List<Leave>) LeaveDAO.getAllLeaveExcludeYou(log, year, period, "None");
                    int i = 0;

                    if (listAll.isEmpty()) {

                %>
                <li class="media align-items-center">
                    Tiada permohonan cuti

                </li>

                <%                                            }

                    for (Leave j : listAll) {
                        i++;

                        CoStaff st = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());

LeaveRequest lr = (LeaveRequest) LeaveDAO.getLeaveRequest(log, j.getLeaveID());


                %>
                <li class="media align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-1 col-sm-12 mb-2">
                                <a class="media-img" href="javascript:;">
                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                </a>
                            </div>
                            <div class="col-lg-10 col-sm-12 mb-2">
                                <div class="media-body d-flex align-items-center">
                                    <div class="flex-1">
                                        <span class="badge badge-default"><span class="badge-<%= LeaveDAO.getBadgeColorTypeLeave(j.getType())%> badge-point"></span> <%= j.getType()%></span>
                                        <span class="badge badge-<%= LeaveDAO.getBadgeColor(j.getStatus())%> "><%= LeaveDAO.getMalayWord2(j.getStatus())%></span>
                                         <%
                                    if(lr != null && !j.getStatus().equals("Rejected")){
                                    %>
                                    <span class="badge badge-primary"><%= LeaveDAO.getMalayWord2(lr.getRequestto()) %></span>
                                    <%
                                        }
                                    %>
                                        <%
                                            if (LeaveDAO.hasComment(log, j.getLeaveID())) {
                                        %>

                                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                                        <%
                                            }
                                        %>
                                        <p></p>
                                        <div class="media-heading"><%= GeneralTerm.capitalizeFirstLetter(st.getName())%> </div><small><%= j.getReason()%></small><br>

                                        <span class="text-muted">Tarikh Permohonan :</span> <strong><%= AccountingPeriod.fullDateMonth(j.getDateapply())%></strong>
                                        <br>
                                        <span class="text-muted">Dari : </span><%= AccountingPeriod.fullDateMonth(j.getDatestart())%> <span class="text-muted">hingga</span> <%= AccountingPeriod.fullDateMonth(j.getDateend())%>
                                        <br>
                                        <span class="text-muted">Bilangan Hari : </span><%= j.getDays()%>
                                    </div>


                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-12">
                                <%
                                    if (LeaveDAO.isLeaveInfoExist(log, st.getStaffid(), j.getDatestart())) {

                                %>

                                <button class="btn btn-sm btn-blue btn-rounded view-modal" id="<%= j.getLeaveID()%>"><i class="fa fa-eye" aria-hidden="true"></i> Lihat</button>&nbsp;

                                <%
                                } else if (!LeaveDAO.isLeaveInfoExist(log, st.getStaffid(), j.getDatestart()) && log.getAccessLevel() == 1) {
                                %>
                                <button class="btn btn-sm btn-danger btn-rounded update-modal" id="<%= j.getLeaveID()%>"><i class="fa fa-pencil" aria-hidden="true"></i><span class="button-text"> Kemaskini Cuti</span></button>&nbsp;

                                <%
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </li>
                <%
                    }
                %>

            </ul>
