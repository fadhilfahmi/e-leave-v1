<%-- 
    Document   : conf_car_modal_add
    Created on : Oct 7, 2019, 1:14:07 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page import="com.lcsb.smartbooking.model.Car"%>
<%@page import="com.lcsb.smartbooking.dao.CarDAO"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    
    Driver c = (Driver) DriverDAO.getDriverInfo(log, request.getParameter("driverID"));

%>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title">View Driver Information</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="flexbox mb-4">
                    <span class="btn-icon-only btn-circle bg-primary-50 text-primary mr-3"><i class="ti-car"></i></span>
                    <div class="flex-1 d-flex">
                        <div class="flex-1">
                            <span class="text-muted mr-2"><%= c.getDriverID()%></span>
                            <div><%= c.getDriverName()%></div>
                        </div>
                        <div class="dropdown">
                            <span class="badge badge-<%= DriverDAO.getBadgeColor(c.getStatus()) %> badge-pill"><%= c.getStatus()%></span>
                        </div>
                    </div>
                </div>

                <div class="flexbox mb-4">
                    <span class="btn-icon-only btn-circle bg-primary-50 text-primary mr-3"><i class="ti-user"></i></span>
                    <div class="flex-1 d-flex">
                        <div class="flex-1">
                            <span class="text-muted mr-2"><%= c.getEmail()%></span>
                            <div><%= c.getStatus()%></div>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-4">
                    Remark : <%//= c.getRemark()%>
                </div>
            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                    <!--<button class="btn btn-primary btn-rounded mr-3">Submit</button>
                    <label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-edit font-20"></i>
                        <input type="file">
                    </label>
                    <label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-image font-20"></i>
                        <input type="file">
                    </label>-->
                    <a class="text-primary" id="editButton" title="<%= c.getDriverID()%>"><i class="la la-edit font-20"></i></a>
                   
                </div>
                <a class="text-danger" id="deleteButton" title="<%= c.getDriverID()%>"><i class="la la-trash font-20"></i></a>
            </div>
        </form>
    </div>
</div>