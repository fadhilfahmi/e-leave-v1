<%-- 
    Document   : book_calendar
    Created on : Nov 7, 2019, 12:41:55 PM
    Author     : fadhilfahmi
--%>
<%@page import="com.lcsb.smartbooking.model.BookingDestination"%>
<%@page import="com.lcsb.smartbooking.model.BookingMaster"%>
<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.Book"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Car"%>
<%@page import="com.lcsb.smartbooking.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");


%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#addnew').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'conf_car_add.jsp');

            return false;
        });

        $('#viewcarmodal').click(function (e) {

            e.preventDefault();
            $.ajax({
                async: false,
                url: "PathController?process=addmodal",
                success: function (result) {
                    //alert(result);
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=viewbooking&id=" + id,
                success: function (result) {
                    $(location).attr('href', 'book_car_view.jsp?id=' + id);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('.deletebutton').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletebooking&bookID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'book_list.jsp');
                    }
                });

            });


            return false;
        });

        

        /* var calendarEl = document.getElementById('calendar');
         
         var calendar = new FullCalendar.Calendar(calendarEl, {
         plugins: [ 'interaction', 'dayGrid' ],
         defaultDate: '2019-08-12',
         editable: true,
         eventLimit: true, // allow "more" link when too many events
         events: [
         {
         title: 'All Day Event',
         start: '2019-08-01'
         },
         {
         title: 'Long Event',
         start: '2019-08-07',
         end: '2019-08-10'
         },
         {
         groupId: 999,
         title: 'Repeating Event',
         start: '2019-08-09T16:00:00'
         },
         {
         groupId: 999,
         title: 'Repeating Event',
         start: '2019-08-16T16:00:00'
         },
         {
         title: 'Conference',
         start: '2019-08-11',
         end: '2019-08-13'
         },
         {
         title: 'Meeting',
         start: '2019-08-12T10:30:00',
         end: '2019-08-12T12:30:00'
         },
         {
         title: 'Lunch',
         start: '2019-08-12T12:00:00'
         },
         {
         title: 'Meeting',
         start: '2019-08-12T14:30:00'
         },
         {
         title: 'Happy Hour',
         start: '2019-08-12T17:30:00'
         },
         {
         title: 'Dinner',
         start: '2019-08-12T20:00:00'
         },
         {
         title: 'Birthday Party',
         start: '2019-08-13T07:00:00'
         },
         {
         title: 'Click for Google',
         url: 'http://google.com/',
         start: '2019-08-28'
         }
         ]
         });
         
         calendar.render();*/


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="flexbox-b mb-5">
                    <span class="mr-4 static-badge badge-pink"><i class="la la-calendar-check-o font-36"></i></span>
                    <div>
                        <h5 class="font-strong">CALENDAR</h5>
                        <div class="text-light">Found 18 Events for this week</div>
                    </div>
                </div>
                <div class="row">
                    <!--<div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">DRAGGABLE EVENTS</div>
                            </div>
                            <div class="ibox-body p-3">
                                <div id="external-events">
                                    <div class="ex-event" data-class="fc-event-primary"><i class="badge-point badge-primary mr-3"></i>Business</div>
                                    <div class="ex-event" data-class="fc-event-warning"><i class="badge-point badge-warning mr-3"></i>Reporting</div>
                                    <div class="ex-event" data-class="fc-event-success"><i class="badge-point badge-success mr-3"></i>Meeting</div>
                                    <div class="ex-event" data-class="fc-event-danger"><i class="badge-point badge-danger mr-3"></i>Important</div>
                                    <div class="ex-event" data-class="fc-event-info"><i class="badge-point badge-info mr-3"></i>Personal</div>
                                    <p class="ml-2 mt-4">
                                        <label class="checkbox checkbox-primary">
                                            <input id="drop-remove" type="checkbox">
                                            <span class="input-span"></span>remove after drop</label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">CALENDAR</div>
                                <!--<button class="btn btn-primary btn-rounded btn-air my-3" data-toggle="modal" data-target="#new-event-modal">
                                    <span class="btn-icon"><i class="la la-plus"></i>New Event</span>
                                </button>-->
                            </div>
                            <div class="ibox-body">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- New Event Dialog-->
                <div class="modal fade" id="new-event-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <form class="modal-content form-horizontal" id="newEventForm" action="javascript:;">
                            <!--<div class="modal-header p-4">
                                <h5 class="modal-title">NEW EVENT</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>-->
                            <div class="modal-body p-4">
                                <div class="form-group mb-4">
                                    <label class="text-muted mb-3">Category</label>
                                    <div>
                                        <label class="radio radio-outline-primary radio-inline check-single" data-toggle="tooltip" data-original-title="General">
                                            <input type="radio" name="category" checked value="fc-event-primary">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-warning radio-inline check-single" data-toggle="tooltip" data-original-title="Payment">
                                            <input type="radio" name="category" value="fc-event-warning">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-success radio-inline check-single" data-toggle="tooltip" data-original-title="Technical">
                                            <input type="radio" name="category" value="fc-event-success">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-danger radio-inline check-single" data-toggle="tooltip" data-original-title="Registration">
                                            <input type="radio" name="category" value="fc-event-danger">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-info radio-inline check-single" data-toggle="tooltip" data-original-title="Security">
                                            <input type="radio" name="category" value="fc-event-info">
                                            <span class="input-span"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <input class="form-control form-control-line" id="new-event-title" type="text" name="title" placeholder="Title">
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">Start:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="new-event-start" type="text" name="start" value="">
                                        </div>
                                    </div>
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">End:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="new-event-end" type="text" name="end" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4 pt-3">
                                    <label class="ui-switch switch-icon mr-3 mb-0">
                                        <input id="new-event-allDay" type="checkbox" checked>
                                        <span></span>
                                    </label>All Day</div>
                            </div>
                            <div class="modal-footer justify-content-start bg-primary-50">
                                <button class="btn btn-primary btn-rounded" id="addEventButton" type="submit">Add event</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End New Event Dialog-->
                <!-- Event Detail Dialog-->
                <div class="modal fade" id="event-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <form class="modal-content form-horizontal" id="eventForm" action="javascript:;">
                            <div class="modal-header p-4">
                                <h5 class="modal-title">EDIT EVENT</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body p-4">
                                <div class="form-group mb-4">
                                    <label class="text-muted mb-3">Category</label>
                                    <div>
                                        <label class="radio radio-outline-primary radio-inline check-single" data-toggle="tooltip" data-original-title="General">
                                            <input type="radio" name="category" checked value="fc-event-primary">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-warning radio-inline check-single" data-toggle="tooltip" data-original-title="Payment">
                                            <input type="radio" name="category" value="fc-event-warning">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-success radio-inline check-single" data-toggle="tooltip" data-original-title="Technical">
                                            <input type="radio" name="category" value="fc-event-success">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-danger radio-inline check-single" data-toggle="tooltip" data-original-title="Registration">
                                            <input type="radio" name="category" value="fc-event-danger">
                                            <span class="input-span"></span>
                                        </label>
                                        <label class="radio radio-outline-info radio-inline check-single" data-toggle="tooltip" data-original-title="Security">
                                            <input type="radio" name="category" value="fc-event-info">
                                            <span class="input-span"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <input class="form-control form-control-line" id="event-title" type="text" name="title" placeholder="Title">
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">Start:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="event-start" type="text" name="start" value="">
                                        </div>
                                    </div>
                                    <div class="col-6 form-group mb-4">
                                        <label class="col-form-label text-muted">End:</label>
                                        <div class="input-group-icon input-group-icon-right">
                                            <span class="input-icon input-icon-right"><i class="fa fa-calendar-check-o"></i></span>
                                            <input class="form-control form-control-line datepicker date" id="event-end" type="text" name="end" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4 pt-3">
                                    <label class="ui-switch switch-icon mr-3 mb-0">
                                        <input id="event-allDay" type="checkbox">
                                        <span></span>
                                    </label>All Day</div>
                            </div>
                            <div class="modal-footer justify-content-between bg-primary-50">
                                <button class="btn btn-primary btn-rounded" id="editEventButton" type="submit">Submit</button>
                                <a class="text-danger" id="deleteEventButton" data-dismiss="modal"><i class="la la-trash font-20"></i></a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Event Detail Dialog-->
            </div>
            <!-- END PAGE CONTENT-->
            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
        
      
    });
</script>
</html>
