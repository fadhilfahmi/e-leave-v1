<%-- 
    Document   : leave_record
    Created on : Feb 25, 2020, 8:39:16 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.model.CoStaffDepartment"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%@page import="com.lcsb.eleave.dao.EstateDAO"%>
<%@page import="com.lcsb.eleave.model.EstateInfo"%>
<%@page import="com.lcsb.eleave.dao.DateAndTime"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String id = request.getParameter("id");

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<link href="./assets/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {



        $("#bilanak").change(function () {
            var value = $(this).val();

            //alert(value);

            for (var i = 0, l = value; i < l; i++) {

                var bilanak = parseInt(i) + 1;

                var htmlst = '<div class="form-group">'
                        + ' <li><label class="form-control-label">Level Pendidikan</label></li>'
                        + ' <div>'
                        + ' <select class="form-control show-tick"  name="educationlevel" data-width="200px">'
                        + ' <option value="sijil">Sijil</option>'
                        + ' <option value="diploma">Diploma</option>'
                        + ' <option value="ijazah">Ijazah</option>'
                        + ' <option value="sarjanamuda">Sarjana Muda</option>'
                        + ' <option value="phd">Ph.D</option>'
                        + ' </select>'
                        + ' </div>'
                        + ' </div>';
               // $("#colanak").html(htmlst);

                $('<p><b>Maklumat Anak No ' + bilanak + '</b></p> <label>Nama :</label><input class="form-control form-control-air" id="namaanak' + bilanak + '" name="namaanak' + bilanak + '" type="text" placeholder="" ><label>No. Kp :</label><input class="form-control form-control-air" id="nokpanak' + bilanak + '" name="nokpanak' + bilanak + '" type="text" placeholder="" ><label>Umur :</label><input class="form-control form-control-air" id="umur' + bilanak + '" name="umur' + bilanak + '" type="text" placeholder="" ><label>Anak Kurang Upaya</label> : <input type="checkbox" id="oku' + bilanak + '" name="oku' + bilanak + '" value="oku"><label for="yes">Ya</label><br> ' + htmlst + '<hr>').appendTo("#colanak");

            }

        });
    });
</script>

<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <div class="page-content fade-in-up">
                <form action="javascript:;" id="saveform">
                    <input class="form-control" type="hidden"  id="staffIDX" name="staffID" value="<%= id%>">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <!--<h5 class="font-strong mb-5">BOOK A CAR</h5>-->
                            <div class="ibox">
                                <div class="ibox-head">
                                    <div class="ibox-title">MAKLUMAT PEGAWAI & KAKITANGAN TAHUN 2020</div>
                                </div>
                                <div class="ibox-body">
                                    <div class="ibox-head">
                                        <div class="ibox-title">Maklumat Peribadi</div>
                                        <label>Seksyen 1</label>
                                    </div>
                                    <br>
                                    <div class="form-group mb-4">
                                        <ol>
                                            <li><label>No. Kad Pengenalan</label></li>
                                            <input class="form-control form-control-air" id="nokp" name="nokp" type="text" placeholder="" >
                                            <div class="form-group" id="date_1">
                                                <li><label class="font-normal">Tarikh Lahir</label></li>
                                                <div class="input-group date">
                                                    <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                                    <input class="form-control" type="text" value="<%= AccountingPeriod.getCurrentTimeStamp()%>" name="dob">
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <li><label>Tempat Lahir</label></li>
                                                <input class="form-control form-control-air" id="birthplace" name="birthplace" type="text" placeholder="" >
                                            </div>
                                            <div class="form-group">
                                                <li><label class="form-control-label">Status Perkahwinan</label></li>
                                                <div>
                                                    <select class="form-control selectpicker show-tick" id="marital" name="marital" data-width="200px">
                                                        <option value="bujang">Bujang</option>
                                                        <option value="berkahwin">Berkahwin</option>
                                                        <option value="janda">Janda</option>
                                                        <option value="duda">Duda</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <li><label class="form-control-label">Jenis Darah</label></li>
                                                <div>
                                                    <select class="form-control selectpicker show-tick" id="blood" name="blood" data-width="200px">
                                                        <option value="A">A</option>
                                                        <option value="B">B</option>
                                                        <option value="AB">AB</option>
                                                        <option value="O">O</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <li><label class="form-control-label">Level Pendidikan</label></li>
                                                <div>
                                                    <select class="form-control selectpicker show-tick" id="educationlevel" name="educationlevel" data-width="200px">
                                                        <option value="sijil">Sijil</option>
                                                        <option value="diploma">Diploma</option>
                                                        <option value="ijazah">Ijazah</option>
                                                        <option value="sarjanamuda">Sarjana Muda</option>
                                                        <option value="phd">Ph.D</option>
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="form-group mb-4">
                                                <li><label>Tempat Bekerja</label></li>
                                                <input class="form-control form-control-air" id="department" name="department" type="text" placeholder="" >
                                            </div>

                                            <div class="form-group mb-4">
                                                <li><label>Alamat Tempat Tinggal</label></li>
                                                <textarea class="form-control" rows="3" name="address" id="address"></textarea>
                                            </div>

                                            <div class="form-group mb-4">
                                                <li><label>Alamat Tetap</label></li>
                                                <textarea class="form-control" rows="3" name="fixaddress" id="fixaddress"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <li><label class="form-control-label">Bangsa</label></li>
                                                <div>
                                                    <select class="form-control selectpicker show-tick" id="race" name="race" data-width="200px">
                                                        <option value="melayu">Melayu</option>
                                                        <option value="cina">Cina</option>
                                                        <option value="india">India</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <li><label class="form-control-label">Agama</label></li>
                                                <div>
                                                    <select class="form-control selectpicker show-tick" id="religion" name="religion" data-width="200px">
                                                        <option value="melayu">Islam</option>
                                                        <option value="buddha">Buddha</option>
                                                        <option value="hindu">Hindu</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <li><label>No. Cukai Pendapatan</label></li>
                                                <input class="form-control form-control-air" id="taxno" name="taxno" type="text" placeholder="" >
                                            </div>
                                            <div class="form-group mb-4">
                                                <li><label>No. EPF</label></li>
                                                <input class="form-control form-control-air" id="noepf" name="noepf" type="text" placeholder="" >
                                            </div>
                                            <div class="form-group mb-4">
                                                <li><label>No. SOCSO</label></li>
                                                <input class="form-control form-control-air" id="nosocso" name="nosocso" type="text" placeholder="" >
                                            </div>
                                            <div class="form-group mb-4">
                                                <li><label>Jawatan Sekarang</label></li>
                                                <input class="form-control form-control-air" id="position" name="position" type="text" placeholder="" >
                                            </div>
                                            <div class="form-group" id="date_1">
                                                <li><label class="font-normal">Tarikh Mula Dilantik di LKPP CORPORATION SDN BHD</label></li>
                                                <div class="input-group date">
                                                    <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                                    <input class="form-control" type="text" value="<%= AccountingPeriod.getCurrentTimeStamp()%>" name="workdate">
                                                </div>
                                            </div>

                                            <div class="ibox-head">
                                                <div class="ibox-title">Maklumat Ahli Keluarga</div>
                                                <label>Seksyen 2</label>
                                            </div>
                                            <br>
                                            <div class="form-group mb-4">
                                                <li> <label>Nama suami / isteri</label></li>
                                                <input class="form-control form-control-air" id="spouse" name="spouse" type="text" placeholder="" >
                                            </div>
                                            <div class="form-group mb-4">
                                                <li> <label>No. Kad Pengenalan</label></li>
                                                <input class="form-control form-control-air" id="nokpspouse" name="nokpspouse" type="text" placeholder="" >
                                            </div>
                                            <div class="form-group">
                                                <li><label class="form-control-label">Pekerjaan</label></li>
                                                <div>
                                                    <select class="form-control selectpicker show-tick" id="spousework" name="spousework" data-width="200px">
                                                        <option value="tidakbekerja">Tidak Bekerja</option>
                                                        <option value="bekerja">Bekerja</option>
                                                        <option value="kerjasendiri">Bekerja Sendiri</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <li> <label>Waris Untuk Dihubungi Jika Kecemasan 1</label></li>
                                                <label>Nama : </label>
                                                <input class="form-control form-control-air" id="namawaris1" name="namawaris1" type="text" placeholder="" >
                                                <label>Hubungan : </label>
                                                <input class="form-control form-control-air" id="hubunganwaris1" name="hubunganwaris1" type="text" placeholder="" >
                                                <label>No. Tel : </label>
                                                <input class="form-control form-control-air" id="notelwaris1" name="notelwaris1" type="text" placeholder="" >
                                                <label>Alamat : </label>
                                                <textarea class="form-control" rows="3" name="alamatwaris1" id="alamatwaris1"></textarea>
                                            </div>

                                            <div class="form-group mb-4">
                                                <li> <label>Waris Untuk Dihubungi Jika Kecemasan 2</label></li>
                                                <label>Nama : </label>
                                                <input class="form-control form-control-air" id="namawaris2" name="namawaris2" type="text" placeholder="" >
                                                <label>Hubungan : </label>
                                                <input class="form-control form-control-air" id="hubunganwaris2" name="hubunganwaris2" type="text" placeholder="" >
                                                <label>No. Tel : </label>
                                                <input class="form-control form-control-air" id="notelwaris2" name="namawaris2" type="text" placeholder="" >
                                                <label>Alamat : </label>
                                                <textarea class="form-control" rows="3" name="alamatwaris2" id="alamatwaris2"></textarea>
                                            </div>
                                            <div class="ibox-head">
                                                <div class="ibox-title">Maklumat anak ( kandung/tiri/angkat )</div>
                                                <label>Seksyen 3</label>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <li><label class="form-control-label">Bilangan semua anak </label></li>
                                                <div>
                                                    <select class="form-control selectpicker show-tick" id="bilanak" name="bilanak" data-width="200px">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <div id="colanak">

                                                </div>
                                                <div id="colanakkp">
                                                </div>
                                                <div id="columuranak">
                                                </div>
                                                <div id="">
                                                </div>
                                                <div id="">
                                                </div>
                                                <div id="anakoku">
                                                </div>
                                            </div>

                                        </ol>
                                        <div class="text-right">
                                            <button class="btn btn-primary btn-air mr-2 savebutton" id="<%= id%>"><i class="fa fa-plus" aria-hidden="true"></i><span class="button-text-approve"> Simpan</span></button>
                                            <button class="btn btn-secondary" id="back"><i class="fa fa-chevron-left" aria-hidden="true"></i><span class="button-text-approve"> Kembali</span></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>    
                </form>
            </div>

            <div id="modalhere"></div>
            <div id="modalpassengerdiv"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $("#reason").focusout(function (e) {

                var fill = $(this).val();

                if (fill != '') {
                    $('#savebutton').prop('disabled', false);
                }



            });

            $("#changedestination").change(function (e) {

                e.preventDefault();
                var type = $(this).val();
                $.ajax({
                    url: "list_destination.jsp?type=" + type,
                    success: function (result) {
                        $('#destinationlist').empty().html(result).hide().fadeIn(300);
                    }
                });
                return false;
            });

            //$( "#otherdestination" ).keypress(function( e ) {
            //$('#destinationlist').on('click', '#otherdestination', function(e){

            $('#destinationlistselected').on('click', '.deletedestination', function (e) {

                var thisid = $(this).attr('id');
                //$(this).closest("li").remove();
                var $k = $(this).closest("li");
                $.ajax({
                    async: true,
                    //data: a,
                    type: 'POST',
                    url: "ProcessController?process=deletedestinationselected&id=" + thisid,
                    success: function (result) {

                        if (result == 1) {
                            $k.remove();
                        } else {

                            //$("#selectdestination").addClass("has-error")
                            // $(e).closest(".form-group").removeClass("has-error")
                        }
                    }
                });
                return false;
            });
            $('#list_destination_selected').on('click', '.viewmodaldate', function (e) {
                e.preventDefault();
                var id = $(this).attr('id');
                $.ajax({
                    async: false,
                    url: "PathController?process=setdatefordestination&id=" + id,
                    success: function (result) {
                        $('#modalhere').empty().html(result).hide().fadeIn(300);
                    }
                });
                $('#myModal').modal('toggle')
                return false;
            });
            $('#list_passenger').on('click', '.viewmodalpassenger', function (e) {
                e.preventDefault();
                var id = $(this).attr('id');
                var sessiondid = $('#sessionid').val();
                $.ajax({
                    async: false,
                    url: "PathController?process=viewmodalpassenger&sessionid=" + sessiondid + "&id=" + id,
                    success: function (result) {
                        $('#modalpassengerdiv').empty().html(result).hide().fadeIn(300);
                    }
                });
            });
        });
    </script>
</body>


<script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>


</html>
