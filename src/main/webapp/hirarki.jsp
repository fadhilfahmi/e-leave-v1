<%-- 
    Document   : hirarki
    Created on : Sep 30, 2020, 4:49:39 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //String balLeave = "";
    double balLeave = 0;

    String curYear = AccountingPeriod.getCurYearByCurrentDate();
    String curMonth = AccountingPeriod.getCurPeriodByCurrentDate();
    String curDate = AccountingPeriod.getCurrentTimeStamp();

    if (LeaveDAO.isLeaveInfoExist(log, log.getUserID(), AccountingPeriod.getCurrentTimeStamp())) {
        balLeave = LeaveDAO.getBalanceEligibleLeave(log, log.getUserID(), AccountingPeriod.getCurYearByCurrentDate());
    }

    double bakiCutiLayak = LeaveDAO.getEligibleLeaveForTheMonth(log, log.getUserID(), curYear, curDate);
    double bakiMC = LeaveDAO.getTotalSickLeaveOfTheYear(log, log.getUserID(), curYear);
%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {



        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });

        $('.gotoleavetab').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavetab&tabid=' + id);

            return false;
        });


        $('.view-modal').click(function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "PathController?process=viewleavemodal&leaveID=" + leaveid + "&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">

                <div class="row mb-4">
                    <div class="col-sm-12 col-lg-12">
                        
                                <%
                                    
                                    CoStaff st = (CoStaff) StaffDAO.getInfo(log, log.getUserID());
                                    CoStaff hd = (CoStaff) StaffDAO.getInfoHead(log, st.getStaffid());
                                    CoStaff sv = (CoStaff) StaffDAO.getInfoSupervisor(log, st.getStaffid());

                                %>

                                <div class="col-sm-12 col-lg-12">

                                    <div class="card overflow-visible mt-5 bg-primary" >

                                        <div class="card-body text-center mt-4">

                                            <img class="img-circle img-bordered card-abs-top-center" src="<%= hd.getImageURL()%>" alt="image" width="60" />
                                            <h6 class="mb-1">
                                                <a class="text-white"><%= hd.getName()%></a>
                                            </h6><small class="text-white"><%= hd.getPosition()%></small><br>
                                            <small class="text-white"><%= hd.getDepartment()%></small>
                                            <p class="mt-2"><span class="badge badge-default">Pelulus</span></p>

                                        </div>
                                    </div>
                                </div>
                                <%
                                    if (!StaffDAO.isSupervisorAsHeadDept(log, st.getStaffid())) {
                                %>
                                <div class="col-sm-12 col-lg-12">
                                    <div class="card overflow-visible mt-5 bg-primary" >

                                        <div class="card-body text-center mt-4">

                                            <img class="img-circle img-bordered card-abs-top-center" src="<%= sv.getImageURL()%>" alt="image" width="60" />
                                            <h6 class="mb-1">
                                                <a class="text-white"><%= sv.getName()%></a>
                                            </h6><small class="text-white"><%= sv.getPosition()%></small><br>
                                            <small class="text-white"><%= sv.getDepartment()%></small>
                                            <p class="mt-2"><span class="badge badge-default">Penyokong</span></p>

                                        </div>
                                    </div>
                                </div>

                                <%
                                    }
                                %>
                                <div class="col-sm-12 col-lg-12">
                                    <div class="card overflow-visible mt-5 bg-primary" >

                                        <div class="card-body text-center mt-4">

                                            <img class="img-circle img-bordered card-abs-top-center" src="<%= log.getImageURL()%>" alt="image" width="60" />
                                            <h6 class="mb-1">
                                                <a class="text-white"><%= st.getName()%></a>
                                            </h6><small class="text-white"><%= st.getPosition()%></small><br>
                                            <small class="text-white"><%= st.getDepartment()%></small>
                                            <p class="mt-2"><span class="badge badge-default">Anda</span></p>

                                        </div>
                                    </div>
                                </div>
                    </div>
                   
                </div>
               
                </div>

                <div id="modalhere"></div>
            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            var data1 = [];
            var data3 = [];

            $.ajax({
                async: false,
                url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Tahunan",
                success: function (result) {
                    // $("#haha").html(result);
                    //console.log(result);
                    //data1 = result;
                    //console.log(data1);
                    var obj = jQuery.parseJSON(result);
                    var i = 0;
                    // console.log(obj);
                    $.each(obj, function (key, value) {
                        i++;
                        console.log(key);
                        data1.push(value);
                        //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                    });
                    console.log(data1);

                }});



            $.ajax({
                async: false,
                url: "ProcessController?moduleid=000000&process=getAnalysis&type=Cuti Sakit",
                success: function (result) {
                    // $("#haha").html(result);
                    //console.log(result);
                    //data1 = result;
                    //console.log(data1);
                    var obj = jQuery.parseJSON(result);
                    var i = 0;
                    // console.log(obj);
                    $.each(obj, function (key, value) {
                        i++;
                        console.log(key);
                        data3.push(value);
                        //data2.push([gd(value.year, value.month, i), value.totaltrans]);
                    });
                    console.log(data3);

                }});

            var ctx = document.getElementById("visitors_chart").getContext("2d");
            var visitors_chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                    datasets: [
                        {
                            label: "Sakit",
                            data: data3,
                            borderColor: 'rgba(117,54,230,0.9)',
                            backgroundColor: 'rgba(117,54,230,0.9)',
                            pointBackgroundColor: 'rgba(117,54,230,0.9)',
                            pointBorderColor: 'rgba(117,54,230,0.9)',
                            borderWidth: 1,
                            pointBorderWidth: 1,
                            pointRadius: 0,
                            pointHitRadius: 30,
                        }, {
                            label: "Tahunan",
                            data: data1,
                            borderColor: 'rgba(104,218,221,1)',
                            backgroundColor: 'rgba(104,218,221,1)',
                            pointBackgroundColor: 'rgba(104,218,221,1)',
                            pointBorderColor: 'rgba(104,218,221,1)',
                            borderWidth: 1,
                            pointBorderWidth: 1,
                            pointRadius: 0,
                            pointHitRadius: 30,
                        },
                    ],
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    showScale: false,
                    scales: {
                        xAxes: [{
                                gridLines: {
                                    display: false,
                                },
                            }],
                        yAxes: [{
                                gridLines: {
                                    display: false,
                                    drawBorder: false,
                                },
                            }]
                    },
                    legend: {
                        labels: {
                            boxWidth: 12
                        }
                    },
                }
            });

            visitors_data = {

                1: {
                    data: [
                        data3,
                        data1,
                    ],
                    labels: ["Jan", "Feb", "Mac", "Apr", "Mei", "Jun", "Jul", "Ogs", "Sep", "Okt", "Nov", "Dis"],
                },
                2: {
                    data: [
                        [2, 1, 4, 7],
                        [6, 2, 1, 3],
                        [3, 1, 2, 5],
                    ],
                    labels: ["Minggu 1", "Minggu 2", "Minggu 3", "Minggu 4"],
                },
            };

            $('#chart_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var id = $(this).attr('data-id');
                if (id && +id in visitors_data) {
                    visitors_chart.data.labels = visitors_data[id].labels;
                    var datasets = visitors_chart.data.datasets;
                    $.each(datasets, function (index, value) {
                        datasets[index].data = visitors_data[id].data[index];
                    });
                }
                visitors_chart.update();
            });
        });
    </script>
</body>
</html>
