<%-- 
    Document   : view_leave_modal
    Created on : Jan 22, 2020, 9:13:33 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.eleave.model.Book"%>
<%@page import="com.lcsb.eleave.dao.BookingDAO"%>
<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%@page import="com.lcsb.eleave.model.Driver"%>
<%@page import="com.lcsb.eleave.dao.DriverDAO"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    

%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

       


    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="savecardestination">
            
            <div class="modal-header p-4">
                <h5 class="modal-title">Permohonan Cuti</h5><span class="badge badge-<%//= LeaveDAO.getBadgeColor(l.getStatus())%>"><%//= LeaveDAO.getMalayWord2(l.getStatus())%></span>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                 
                    <!--<label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-edit font-20"></i>
                        <input type="file">
                    </label>
                    <label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-image font-20"></i>
                        <input type="file">
                    </label>-->


                </div>
                <button class="btn btn-blue btn-rounded mr-3 viewdetail pull-right" title="Ask" id="<//%=l.getLeaveID()%>"><i class="fa fa-expand" ></i></button>
            </div>
            <div class="modal-body p-4">
                <div class="col-sm-12 col-lg-12">
                        <div class="card overflow-visible mt-5 bg-primary" >
                            <div class="card-body text-center mt-4">
                                <img class="img-circle img-bordered card-abs-top-center" src="<%= log.getImageURL()%>" alt="image" width="60" />
                                <h6 class="mb-1">
                                    <a class="text-white"><%= log.getFullname()%></a>
                                </h6><small class="text-white"><%= StaffDAO.getInfo(log, log.getUserID()).getPosition()%></small><br>
                                <small class="text-white"><%= StaffDAO.getInfo(log, log.getUserID()).getDepartment()%></small>
                                
                            </div>
                        </div>
                    </div>
                <div class="ibox">
                    <div class="ibox-body">
                        <!--<h5 class="font-strong mb-4"><%//= st.getName()%></h5>-->
                        

                        <div class="row">

                         
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-body">
                       

                        
                          

                    </div>
                </div>


            </div>

        </form>
    </div>
</div>