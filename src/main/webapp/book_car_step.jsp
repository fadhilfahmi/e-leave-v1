<%-- 
    Document   : book_car_step
    Created on : Oct 24, 2019, 12:06:41 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.dao.EstateDAO"%>
<%@page import="com.lcsb.smartbooking.model.EstateInfo"%>
<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String sessionid = request.getParameter("sessionid");

%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".select2_demo_1").select2();

        $("#savebutton").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();

            var a = $("#saveform :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=savebook",
                success: function (result) {
                    $(location).attr('href', 'book_list.jsp');
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


        $("#back").click(function () {
            parent.history.back();
            return false;
        });

        $('#date_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('.clockpicker').clockpicker();

        $(".select2_demo_2").select2({
            placeholder: "Select a state",
            allowClear: true
        });

        $('#gettypereceiver').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Receiver',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_receiver.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });



    });

</script>

<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <div class="page-content fade-in-up">

                <!--<h5 class="font-strong mb-5">BOOK A CAR</h5>-->
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Steps with validation</div>
                    </div>
                    <div class="ibox-body">
                        <form id="form-wizard" action="javascript:;" novalidate="novalidate">
                            <input type="hidden" id="sessionid" value="<%=sessionid%>">
                            <h6>Destination</h6>
                            <section>
                                <h3>Select your destination.</h3>
                                <!--<div class="row">
                                    <div class="col-sm-12 form-group mb-4">
                                        <div class="form-group mb-4">
                                            <label>Destination Type</label>
                                            <div class="mt-1">
                                                <label class="radio radio-inline radio-grey radio-primary">
                                                    <input type="radio" name="d" checked>
                                                    <span class="input-span"></span>Single Destination</label>
                                                <label class="radio radio-inline radio-grey radio-primary">
                                                    <input type="radio" name="d">
                                                    <span class="input-span"></span>Multi Destination</label>
                                            </div>
                                            <span class="help-block">Select one of 2 types of destinations.</span>
                                        </div>
                                    </div>
                                </div>-->


                                <div class="row">
                                    <div class="col-sm-4 form-group mb-4">
                                        <div class="form-group mb-4">
                                            <label class="font-normal">Type</label>
                                            <select class="selectpicker show-tick form-control" title="Please select" data-style="btn-solid" name="status" id="changedestination">
                                                <option value="Estate">Estates</option>
                                                <option value="Mill">Mill</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 form-group mb-4" id="destinationlist">
                                        <!-- list destination --> 
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 form-group mb-4">
                                        <!-- list destination --> 
                                        <ul class="media-list media-list-divider mr-2 scroller"  id="destinationlistselected">


                                        </ul>
                                    </div>
                                </div>
                            </section>
                            <h6>Date & Time</h6>
                            <section>
                                <h3>Select date & time prior to destination.</h3>
                                <div class="row" id="list_destination_selected">
                                </div>
                            </section>
                            <h6>Passenger</h6>
                            <section>
                                <h3>Select passenger for this ride.</h3>
                                <div class="row" id="list_passenger">
                                </div>
                            </section>
                            <h6>Confirmation</h6>
                            <section>
                                <h3>Summary of your booking information</h3>
                                <div class="row" id="booking-summary">
                                </div>
                            </section>
                        </form>
                    </div>
                </div>


            </div>

            <div id="modalhere"></div>
            <div id="modalpassengerdiv"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#form-wizard').steps({
                headerTag: "h6",
                bodyTag: "section",
                titleTemplate: '<span class="step-number">#index#</span> #title#',
                onStepChanging: function (event, currentIndex, newIndex) {
                    var form = $(this);
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex) {
                        return true;
                    }

                    if (currentIndex == 0) {
                        var sessionid = $('#sessionid').val();
                        $.ajax({
                            url: "list_destination_selected.jsp?sessionid=" + sessionid,
                            success: function (result) {
                                $('#list_destination_selected').empty().html(result).hide().fadeIn(300);
                            }
                        });
                        //return false;
                    }

                    if (currentIndex == 1) {
                        var sessionid = $('#sessionid').val();
                        $.ajax({
                            url: "list_passenger.jsp?sessionid=" + sessionid,
                            success: function (result) {
                                $('#list_passenger').empty().html(result).hide().fadeIn(300);
                            }
                        });
                        //return false;
                    }
                    if (currentIndex == 2) {
                        var sessionid = $('#sessionid').val();
                        $.ajax({
                            url: "booking_summary.jsp?sessionid=" + sessionid,
                            success: function (result) {
                                $('#booking-summary').empty().html(result).hide().fadeIn(300);
                            }
                        });
                        //return false;
                    }
                    // Clean up if user went backward before
                    if (currentIndex < newIndex) {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onFinishing: function (event, currentIndex) {
                    var form = $(this);
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    toastr.success('Submitted!');
                    var sessionid = $('#sessionid').val();
                    $.ajax({
                        url: "PathController?process=savebookmaster&sessionid=" + sessionid,
                        success: function (result) {
                            //$(location).attr('href', 'book_list.jsp');
                        }
                    });

                    var email = 'fadhil.fahmi@gmail.com';
                    var subject = 'send from nowhere';
                    var body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> <html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" > <title>Mailto</title> <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet"> <style type="text/css"> html { -webkit-text-size-adjust: none; -ms-text-size-adjust: none;}@media only screen and (min-device-width: 750px) { .table750 {width: 750px !important;} } @media only screen and (max-device-width: 750px), only screen and (max-width: 750px){ table[class="table750"] {width: 100% !important;} .mob_b {width: 93% !important; max-width: 93% !important; min-width: 93% !important;} .mob_b1 {width: 100% !important; max-width: 100% !important; min-width: 100% !important;} .mob_left {text-align: left !important;} .mob_soc {width: 50% !important; max-width: 50% !important; min-width: 50% !important;} .mob_menu {width: 50% !important; max-width: 50% !important; min-width: 50% !important; box-shadow: inset -1px -1px 0 0 rgba(255, 255, 255, 0.2); } .mob_btn {width: 100% !important; max-width: 100% !important; min-width: 100% !important;} .mob_card {width: 88% !important; max-width: 88% !important; min-width: 88% !important;} .mob_title1 {font-size: 36px !important; line-height: 40px !important;} .mob_title2 {font-size: 26px !important; line-height: 33px !important;} .top_pad {height: 15px !important; max-height: 15px !important; min-height: 15px !important;} .mob_pad {width: 15px !important; max-width: 15px !important; min-width: 15px !important;} .top_pad2 {height: 40px !important; max-height: 40px !important; min-height: 40px !important;} } @media only screen and (max-device-width: 550px), only screen and (max-width: 550px){ .mod_div {display: block !important;} } .table750 {width: 750px;} </style> </head> <body style="margin: 0; padding: 0;"><table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #f5f8fa; min-width: 340px; font-size: 1px; line-height: normal;"> <tr> <td align="center" valign="top"> <!--[if (gte mso 9)|(IE)]> <table border="0" cellspacing="0" cellpadding="0"> <tr><td align="center" valign="top" width="750"><![endif]--> <table cellpadding="0" cellspacing="0" border="0" width="750" class="table750" style="width: 100%; max-width: 750px; min-width: 340px; background: #f5f8fa;"> <tr> <td class="mob_pad" width="25" style="width: 25px; max-width: 25px; min-width: 25px;">&nbsp;</td> <td align="center" valign="top" style="background: #ffffff;"><table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;"> <tr> <td align="right" valign="top"> <div class="top_pad" style="height: 25px; line-height: 25px; font-size: 23px;">&nbsp;</div> </td> </tr> </table><table cellpadding="0" cellspacing="0" border="0" width="88%" style="width: 88% !important; min-width: 88%; max-width: 88%;"> <tr> <td align="left" valign="top"> <div style="height: 30px; line-height: 30px; font-size: 28px;">&nbsp;</div> <!--[if (gte mso 9)|(IE)]> <table border="0" cellspacing="0" cellpadding="0"> <tr><td align="center" valign="top" width="460"><![endif]--> <div style="display: inline-block; vertical-align: top; width: 74%; min-width: 270px;"> <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100% !important; min-width: 100%; max-width: 100%;"> <tr> <td align="left" valign="top"> <div style="height: 10px; line-height: 10px; font-size: 20px;">&nbsp;</div> <a href="#" target="_blank" style="display: block; max-width: 128px;"> <img src="https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP" alt="img" width="40" border="0" style="display: block; width: 40;" /> </a> </td> </tr> </table> </div><!--[if (gte mso 9)|(IE)]></td><td align="center" valign="top" width="150"><![endif]--><div style="display: inline-block; vertical-align: top; width: 150px;"> <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100% !important; min-width: 100%; max-width: 100%;"> <tr> <td class="mob_left" align="right" valign="top"> <div style="height: 15px; line-height: 15px; font-size: 13px;">&nbsp;</div> <img src="https://docs.google.com/uc?id=1oJiHf9R7Aeby8zwlIGmVU7A9BTtl9zd2" alt="img" width="200" border="0" style="display: block; width: 200;" /> </td> </tr> </table> </div> <!--[if (gte mso 9)|(IE)]> </td></tr> </table><![endif]--> <div class="top_pad2" style="height: 70px; line-height: 70px; font-size: 68px;">&nbsp;</div> </td> </tr> </table><table cellpadding="0" cellspacing="0" border="0" width="88%" style="width: 88% !important; min-width: 88%; max-width: 88%;"> <tr> <td align="left" valign="top"> <font class="mob_title1" face="\'Source Sans Pro\', sans-serif" color="#1a1a1a" style="font-size: 52px; line-height: 55px; font-weight: 300; letter-spacing: -1.5px;"> <span class="mob_title1" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 52px; line-height: 55px; font-weight: 300; letter-spacing: -1.5px;">Financial Management System</span> </font> <div style="height: 22px; line-height: 22px; font-size: 20px;">&nbsp;</div> <font class="mob_title2" face="\'Source Sans Pro\', sans-serif" color="#5e5e5e" style="font-size: 36px; line-height: 45px; font-weight: 300; letter-spacing: -1px;"> <span class="mob_title2" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #5e5e5e; font-size: 36px; line-height: 45px; font-weight: 300; letter-spacing: -1px;">Hi Mohd Fadhil Fahmi</span> </font> </td> </tr> </table><table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100% !important; min-width: 100%; max-width: 100%;"> <tr> <td align="center" valign="top"> <!--[if (gte mso 9)|(IE)]> <table border="0" cellspacing="0" cellpadding="0"> <tr><td align="center" valign="top" width="325"><![endif]--> <div class="mob_btn" style="display: inline-block; vertical-align: top; width: 325px;"> <table class="mob_card" cellpadding="0" cellspacing="0" border="0" width="295" style="width: 295px !important; min-width: 295px; max-width: 295px;"> <tr> <td align="left" valign="top"> <div style="height: 35px; line-height: 35px; font-size: 33px;">&nbsp;</div><div style="height: 22px; line-height: 22px; font-size: 20px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#3f51b5" style="font-size: 16px; line-height: 22px; font-weight: 700; text-transform: uppercase;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #3f51b5; font-size: 16px; line-height: 22px; font-weight: 700; text-transform: uppercase;">Email campaigns</span> </font> <div style="height: 12px; line-height: 12px; font-size: 10px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#101010" style="font-size: 26px; line-height: 33px; font-weight: 300; letter-spacing: -1px;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #101010; font-size: 26px; line-height: 33px; font-weight: 300; letter-spacing: -1px;">Effective email marketing techniques backed by data.</span> </font> <div style="height: 12px; line-height: 12px; font-size: 10px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#767676" style="font-size: 17px; line-height: 21px;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #767676; font-size: 17px; line-height: 21px;">230 comments&nbsp;&nbsp;&bull;&nbsp;&nbsp;54,000 views</span> </font> <div style="height: 8px; line-height: 8px; font-size: 6px;">&nbsp;</div> </td> </tr> </table> </div><!--[if (gte mso 9)|(IE)]></td><td align="center" valign="top" width="325"><![endif]--><div class="mob_btn" style="display: inline-block; vertical-align: top; width: 325px;"> <table class="mob_card" cellpadding="0" cellspacing="0" border="0" width="295" style="width: 295px !important; min-width: 295px; max-width: 295px;"> <tr> <td align="left" valign="top"> <div style="height: 35px; line-height: 35px; font-size: 33px;">&nbsp;</div><div style="height: 22px; line-height: 22px; font-size: 20px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#ff9800" style="font-size: 16px; line-height: 22px; font-weight: 700; text-transform: uppercase;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #ff9800; font-size: 16px; line-height: 22px; font-weight: 700; text-transform: uppercase;">Marketing</span> </font> <div style="height: 12px; line-height: 12px; font-size: 10px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#101010" style="font-size: 26px; line-height: 33px; font-weight: 300; letter-spacing: -1px;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #101010; font-size: 26px; line-height: 33px; font-weight: 300; letter-spacing: -1px;">Elements of a good email permission reminder.</span> </font> <div style="height: 12px; line-height: 12px; font-size: 10px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#767676" style="font-size: 17px; line-height: 21px;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #767676; font-size: 17px; line-height: 21px;">60 comments&nbsp;&nbsp;&bull;&nbsp;&nbsp;25,000 views</span> </font> <div style="height: 8px; line-height: 8px; font-size: 6px;">&nbsp;</div> </td> </tr> </table> </div> <!--[if (gte mso 9)|(IE)]> </td></tr> </table><![endif]--> </td> </tr> </table><table cellpadding="0" cellspacing="0" border="0" width="88%" style="width: 88% !important; min-width: 88%; max-width: 88%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;"> <tr> <td align="center" valign="top"> <div style="height: 40px; line-height: 40px; font-size: 38px;">&nbsp;</div> <table class="mob_btn" cellpadding="0" cellspacing="0" border="0" style="background: #27cbcc; border-radius: 4px;"> <tr> <td align="center" valign="top"> <a href="#" target="_blank" style="display: block; border: 1px solid #27cbcc; border-radius: 4px; padding: 19px 26px; font-family: \'Source Sans Pro\', Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 26px; line-height: 30px; text-decoration: none; white-space: nowrap; font-weight: 600;"> <font face="\'Source Sans Pro\', sans-serif" color="#ffffff" style="font-size: 26px; line-height: 30px; text-decoration: none; white-space: nowrap; font-weight: 600;"> <span style="font-family: \'Source Sans Pro\', Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 26px; line-height: 30px; text-decoration: none; white-space: nowrap; font-weight: 600;">Go To FMS</span> </font> </a> </td> </tr> </table> <div style="height: 50px; line-height: 50px; font-size: 48px;">&nbsp;</div> </td> </tr> </table><table cellpadding="0" cellspacing="0" border="0" width="88%" style="width: 88% !important; min-width: 88%; max-width: 88%;"> <tr> <td class="mob_left" align="center" valign="top"> <div style="height: 25px; line-height: 25px; font-size: 23px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#767676" style="font-size: 17px; line-height: 23px;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #767676; font-size: 17px; line-height: 23px;">From time to time, we send new hot collections to all members of Mailto. We hope you find them useful. If not, you can <a href="#" target="_blank" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 23px; text-decoration: none;">unsubscribe</a> from our digest.</span> </font> <div style="height: 28px; line-height: 28px; font-size: 26px;">&nbsp;</div> </td> </tr> </table><table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;"> <tr> <td align="center" valign="top"> <div style="height: 34px; line-height: 34px; font-size: 32px;">&nbsp;</div> <table cellpadding="0" cellspacing="0" border="0" width="88%" style="width: 88% !important; min-width: 88%; max-width: 88%;"> <tr> <td align="center" valign="top"> <table cellpadding="0" cellspacing="0" border="0" width="78%" style="min-width: 300px;"> <tr> <td align="center" valign="top" width="23%"> <a href="#" target="_blank" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;"> <font face="\'Source Sans Pro\', sans-serif" color="#1a1a1a" style="font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;">HELP&nbsp;CENTER</span> </font> </a> </td> <td align="center" valign="top" width="10%"> <font face="\'Source Sans Pro\', sans-serif" color="#1a1a1a" style="font-size: 17px; line-height: 17px; font-weight: bold;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 17px; font-weight: bold;">&bull;</span> </font> </td> <td align="center" valign="top" width="23%"> <a href="#" target="_blank" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;"> <font face="\'Source Sans Pro\', sans-serif" color="#1a1a1a" style="font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;">SUPPORT&nbsp;24/7</span> </font> </a> </td> <td align="center" valign="top" width="10%"> <font face="\'Source Sans Pro\', sans-serif" color="#1a1a1a" style="font-size: 17px; line-height: 17px; font-weight: bold;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 17px; font-weight: bold;">&bull;</span> </font> </td> <td align="center" valign="top" width="23%"> <a href="#" target="_blank" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;"> <font face="\'Source Sans Pro\', sans-serif" color="#1a1a1a" style="font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 14px; line-height: 20px; text-decoration: none; white-space: nowrap; font-weight: bold;">ACCOUNT</span> </font> </a> </td> </tr> </table> <div style="height: 34px; line-height: 34px; font-size: 32px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#868686" style="font-size: 17px; line-height: 20px;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;">Copyright &copy; 2017 Mailto. All&nbsp;Rights&nbsp;Reserved. We&nbsp;appreciate&nbsp;you!</span> </font> <div style="height: 3px; line-height: 3px; font-size: 1px;">&nbsp;</div> <font face="\'Source Sans Pro\', sans-serif" color="#1a1a1a" style="font-size: 17px; line-height: 20px;"> <span style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;"><a href="#" target="_blank" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;">help@mailto.com</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#" target="_blank" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;">1(800)232-90-26</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#" target="_blank" style="font-family: \'Source Sans Pro\', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;">Unsubscribe</a></span> </font> <div style="height: 35px; line-height: 35px; font-size: 33px;">&nbsp;</div> <table cellpadding="0" cellspacing="0" border="0"> <tr> <td align="center" valign="top"> <a href="#" target="_blank" style="display: block; max-width: 19px;"> <img src="img/soc_1.png" alt="img" width="19" border="0" style="display: block; width: 19px;" /> </a> </td> <td width="45" style="width: 45px; max-width: 45px; min-width: 45px;">&nbsp;</td> <td align="center" valign="top"> <a href="#" target="_blank" style="display: block; max-width: 18px;"> <img src="img/soc_2.png" alt="img" width="18" border="0" style="display: block; width: 18px;" /> </a> </td> <td width="45" style="width: 45px; max-width: 45px; min-width: 45px;">&nbsp;</td> <td align="center" valign="top"> <a href="#" target="_blank" style="display: block; max-width: 21px;"> <img src="img/soc_3.png" alt="img" width="21" border="0" style="display: block; width: 21px;" /> </a> </td> <td width="45" style="width: 45px; max-width: 45px; min-width: 45px;">&nbsp;</td> <td align="center" valign="top"> <a href="#" target="_blank" style="display: block; max-width: 25px;"> <img src="img/soc_4.png" alt="img" width="25" border="0" style="display: block; width: 25px;" /> </a> </td> </tr> </table> <div style="height: 35px; line-height: 35px; font-size: 33px;">&nbsp;</div> </td> </tr> </table> </td> </tr> </table></td> <td class="mob_pad" width="25" style="width: 25px; max-width: 25px; min-width: 25px;">&nbsp;</td> </tr> </table> <!--[if (gte mso 9)|(IE)]> </td></tr> </table><![endif]--> </td> </tr> </table> </body> </html>';

                    
                    $.ajax({
                        type: "POST",
                        url: "http://localhost:8081/send",
                        //url: "PathController?process=savebookmaster&sessionid=" + sessionid,
                        // The key needs to match your method's input parameter (case-sensitive).
                        data: JSON.stringify({"to_address": email, "subject": subject, "body": body}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            alert(data);
                        },
                        failure: function (errMsg) {
                            alert(errMsg);
                        }
                    });

                }
            }).validate({
                errorPlacement: function errorPlacement(error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                },
                errorClass: "help-block error",
                highlight: function (e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function (e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });



            $("#changedestination").change(function (e) {

                e.preventDefault();
                var type = $(this).val();
                $.ajax({
                    url: "list_destination.jsp?type=" + type,
                    success: function (result) {
                        $('#destinationlist').empty().html(result).hide().fadeIn(300);
                    }
                });
                return false;
            });

            $('#destinationlist').on('change', '#selectdestination', function (e) {

                var destcode = $(this).val();
                var destdescp = ($(this).find(':selected').data('descp'));
                var desttype = ($(this).find(':selected').data('type'));

                //var a = $("#saveform :input").serialize();
                $.ajax({
                    async: true,
                    //data: a,
                    type: 'POST',
                    url: "ProcessController?process=savedestinationselected&desttype=" + desttype + "&destcode=" + destcode + "&destdescp=" + destdescp + "&sessionid=<%=sessionid%>",
                    success: function (result) {
                        if (result > 0) {
                            var appendthis = '<li class="media align-items-center" id="lielement' + result + '">'
                                    //+ '<a class="media-img" href="javascript:;">'
                                    //+ '<img class="img-circle" src="./assets/img/users/u6.jpg" alt="image" width="54" />'
                                    //+ '</a>'
                                    + '<div class="media-body d-flex align-items-center">'
                                    + '<div class="flex-1">'
                                    + '<div class="media-heading">' + desttype + '</div><small class="text-muted">' + destdescp + '</small></div>'
                                    + '<button class="btn btn-sm btn-danger btn-rounded deletedestination" id="' + result + '">Delete</button>'
                                    + ' </div>'
                                    + ' </li>';
                            $('#destinationlistselected').append(appendthis);

                        } else {

                            $("#selectdestination").addClass("has-error")
                            // $(e).closest(".form-group").removeClass("has-error")
                        }

                        $('#selectdestination').prop('selectedIndex', 0);
                    }
                });
                return false;


            });




            //$( "#otherdestination" ).keypress(function( e ) {
            //$('#destinationlist').on('click', '#otherdestination', function(e){
            $('#destinationlist').on('click', '#enterdestination', function (e) {


                //var dest = $(this : input).closest('input').find("input[name='otherdestination']").val();

                var dest = $('#otherdestination').val();

                $.ajax({
                    async: true,
                    //data: a,
                    type: 'POST',
                    url: "ProcessController?process=savedestinationselected&desttype=Others&destcode=None&destdescp=" + dest + "&sessionid=<%=sessionid%>",
                    success: function (result) {
                        if (result > 0) {
                            var appendthis = '<li class="media align-items-center" id="lielement' + result + '">'
                                    //+ '<a class="media-img" href="javascript:;">'
                                    //+ '<img class="img-circle" src="./assets/img/users/u6.jpg" alt="image" width="54" />'
                                    //+ '</a>'
                                    + '<div class="media-body d-flex align-items-center">'
                                    + '<div class="flex-1">'
                                    + '<div class="media-heading">Others</div><small class="text-muted">' + dest + '</small></div>'
                                    + '<button class="btn btn-sm btn-danger btn-rounded deletedestination" id="' + result + '">Delete</button>'
                                    + ' </div>'
                                    + ' </li>';
                            $('#destinationlistselected').append(appendthis);

                        } else {


                        }


                    }
                });

                return false;

            });

            $('#destinationlistselected').on('click', '.deletedestination', function (e) {

                var thisid = $(this).attr('id');
                //$(this).closest("li").remove();
                var $k = $(this).closest("li");

                $.ajax({
                    async: true,
                    //data: a,
                    type: 'POST',
                    url: "ProcessController?process=deletedestinationselected&id=" + thisid,
                    success: function (result) {

                        if (result == 1) {
                            $k.remove();

                        } else {

                            //$("#selectdestination").addClass("has-error")
                            // $(e).closest(".form-group").removeClass("has-error")
                        }
                    }
                });

                return false;

            });

            $('#list_destination_selected').on('click', '.viewmodaldate', function (e) {
                e.preventDefault();

                var id = $(this).attr('id');
                $.ajax({
                    async: false,
                    url: "PathController?process=setdatefordestination&id=" + id,
                    success: function (result) {
                        $('#modalhere').empty().html(result).hide().fadeIn(300);
                    }
                });


                $('#myModal').modal('toggle')
                return false;
            });

            $('#list_passenger').on('click', '.viewmodalpassenger', function (e) {
                e.preventDefault();

                var id = $(this).attr('id');
                var sessiondid = $('#sessionid').val();
                $.ajax({
                    async: false,
                    url: "PathController?process=viewmodalpassenger&sessionid=" + sessiondid + "&id=" + id,
                    success: function (result) {
                        $('#modalpassengerdiv').empty().html(result).hide().fadeIn(300);
                    }
                });


                $('#modalpassenger').modal('toggle');
                return false;
            });



        });
    </script>
</body>
</html>
