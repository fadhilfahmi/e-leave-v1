<%-- 
    Document   : conf_car_modal_add
    Created on : Oct 7, 2019, 1:14:07 PM
    Author     : fadhilfahmi
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.smartbooking.model.Book"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.dao.ParameterDAO"%>
<%@page import="com.lcsb.smartbooking.model.Driver"%>
<%@page import="com.lcsb.smartbooking.dao.DriverDAO"%>
<%@page import="com.lcsb.smartbooking.model.Car"%>
<%@page import="com.lcsb.smartbooking.dao.CarDAO"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //Car c = (Car) CarDAO.getCarInfo(log, request.getParameter("carID"));
    //Driver d = (Driver) DriverDAO.getDriverInfo(log, c.getDriverID());
    Book b = (Book) BookingDAO.getBookingInfo(log, request.getParameter("bookID"));
    String destID = request.getParameter("id");

%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        
        $(".selectcar").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();
            var id = $(this).attr('id');
            var destid = $('#destID').val();
            var a = $("#savecardestination :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "ProcessController?process=assigncarfordestination",
                success: function (result) {
                    console.log(result);
                    if(result > 0){
                        $(location).attr('href', 'book_car_view.jsp?id='+id);
                    }
                    
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });
        
        
    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="savecardestination">
            <input type="hidden" name="destID" id="destID" value="<%= destID %>">
            <input type="hidden" name="bookID" id="bookID" value="<%= b.getBookID() %>">
            <div class="modal-header p-4">
                <h5 class="modal-title">Assign Car</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="flexbox mb-4">
                    <div class="flex-1 d-flex">
                        <div class="flex-1">
                            <div class="col-sm-12 form-group mb-4">
                                <div>
                                    <label>Driven Type</label>
                                    <select class="show-tick form-control" title="Please select" data-style="btn-solid" name="drivetype" id="drivetype">

                                        <%= ParameterDAO.parameterList(log, "Drive Type", "")%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group mb-4">
                                <label>Car</label><small> *List of available car on that period</small>
                                <select class="show-tick form-control" title="Please select" data-style="btn-solid" name="carID" id="selectcar">
                                    <%
                                    ArrayList<String> ar = BookingDAO.getAvailableCarIDinRangePeriod(log, b.getBookID(), destID);
                                    for (String i : ar) {
                                         Car c = (Car) CarDAO.getCarInfo(log, i);
                                         
                                         String withDriver = "";
                                         if(!c.getDriverID().equals("Self Drive")){
                                             withDriver = "[Driver]";
                                         }
                                    %>
                                    <option value="<%= c.getCarID() %>"><%= c.getCarID() %> - <%= c.getDescp()%> <%= withDriver %></option>
                                    
                                    <%
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                    <button class="btn btn-primary btn-rounded mr-3 selectcar" id="<%=b.getBookID()%>">Assign</button>
                    <!--<label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-edit font-20"></i>
                        <input type="file">
                    </label>
                    <label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-image font-20"></i>
                        <input type="file">
                    </label>-->
                   

                </div>
            </div>
        </form>
    </div>
</div>