<%-- 
    Document   : list_receiver_by
    Created on : Mar 17, 2016, 2:54:36 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ReceiverDAO"%>
<%@page import="com.lcsb.fms.util.model.Receiver"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String by = request.getParameter("by");
            String keyword = request.getParameter("keyword");
            
            List<Receiver> slist = (List<Receiver>) ReceiverDAO.getReceiver(log, by,keyword);
            for (Receiver c : slist) { 
               %>
       
            <a href="<%= c.getReceiverDesc()%>" id="<%= c.getReceiverCode()%>" class="list-group-item thisresult_nd">
                <div id="left_div"><%= c.getReceiverCode()%></div>
                <div id="right_div"><%= c.getReceiverDesc()%></div>
            </a>

               <%    
            } 
        %>
</div>