<%-- 
    Document   : view_leave_approval
    Created on : Jan 28, 2020, 3:30:48 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.eleave.dao.NotifyDAO"%>
<%@page import="com.lcsb.eleave.model.LeaveComment"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.dao.ParameterDAO"%>
<%@page import="com.lcsb.eleave.dao.EstateDAO"%>
<%@page import="com.lcsb.eleave.model.EstateInfo"%>
<%@page import="com.lcsb.eleave.dao.DateAndTime"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Driver"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.eleave.dao.DriverDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (log != null) {

        //Car c = (Car) CarDAO.getCarInfo(log, request.getParameter("carID"));
        //Driver d = (Driver) DriverDAO.getDriverInfo(log, c.getDriverID());
        Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));

        if (l != null) {

            CoStaff st = (CoStaff) StaffDAO.getInfo(log, l.getStaffID());
            String destID = request.getParameter("id");

            //String yearApply = AccountingPeriod.getCurYearByDate(AccountingPeriod.getCurrentTimeStamp());
            //String monthApply = AccountingPeriod.getCurPeriodByDate(AccountingPeriod.getCurrentTimeStamp());
            String yearApply = AccountingPeriod.getCurYearByDate(l.getDatestart());
            String monthApply = AccountingPeriod.getCurPeriodByDate(l.getDatestart());

            int totLeaveUse = LeaveDAO.getTotalLeaveOfTheYear(log, st.getStaffid(), yearApply);
            int totMedicalLeaveUse = LeaveDAO.getTotalSickLeaveUseOfTheYear(log, st.getStaffid(), yearApply);
            int totLeaveUseMonth = LeaveDAO.getTotalLeaveOfTheMonth(log, st.getStaffid(), yearApply, monthApply);
            double totLeaveYear = LeaveDAO.getEligibleLeaveOfTheYear(log, st.getStaffid(), yearApply);
            double totMedicalLeaveYear = LeaveDAO.getMedicalLeaveOfTheYear(log, st.getStaffid(), yearApply);
            double percentYear = LeaveDAO.getPercentLeaveUseYear(log, st.getStaffid(), yearApply);
            double percentYearMedical = LeaveDAO.getPercentMedicalLeaveUseYear(log, st.getStaffid(), yearApply);
            double percentMonth = LeaveDAO.getPercentLeaveUseMonth(log, st.getStaffid(), yearApply, monthApply, l.getDatestart());

            String buttonValid = "";
            String titleValid = "";

            if (log.getAccessLevel() == 2) {

                buttonValid = "Sokong";
                titleValid = "Supported";
            } else if (log.getAccessLevel() == 1) {

                buttonValid = "Semak";
                titleValid = "Checked";
            } else if (log.getAccessLevel() == 3) {

                if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("support")) {
                    buttonValid = "Sokong";
                    titleValid = "Supported";
                } else if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("approve")) {
                    buttonValid = "Lulus";
                    titleValid = "Approved";
                }

            } else if (log.getAccessLevel() == 4) {

                buttonValid = "Sah";
                titleValid = "Verified";
            }

            //NotifyDAO.updateNotifyFromPage(log, l.getLeaveID());

%>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $.ajax({
            url: "leave_comment.jsp?leaveID=<%= l.getLeaveID()%>",
            success: function (result) {
                $('#comment-section').empty().html(result).hide().fadeIn(300);
            }
        });

        $(".actionto").unbind('click').bind('click', function (e) {

            $(this).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Tunggu ... ');
//var a = $("form").serialize();
            var id = $(this).attr('id');
            var action = $(this).attr('title');

            var stringnoti = 'disahkan';

            if (action == 'Rejected') {
                stringnoti = 'dibatalkan';
            } else if (action == 'Approved') {
                stringnoti = 'diluluskan';
            } else if (action == 'Supported') {
                stringnoti = 'disokong';
            }


            $.ajax({
                async: true,
                url: "PathController?process=validateleave&leaveID=" + id + "&action=" + action,
                success: function (result) {
                    //if (result > 0) {

                    //}

                    if (action == 'Rejected') {
                        alertify
                                .defaultValue("")
                                .prompt("Nyatakan sebab anda tolak permohonan ini.",
                                        function (val, ev) {
                                            ev.preventDefault();
                                            $.ajax({
                                                async: true,
                                                url: "PathController?process=leavecomment&leaveID=" + id + "&comment=" + val,
                                                success: function (result) {
                                                    //if (result > 0) {

                                                    //}


                                                    alertify.success("Mesej anda telah disimpan.");
                                                    //$(location).attr('href', 'leave_list_approval.jsp?tab=2');
                                                    $.ajax({
                                                        url: "leave_comment.jsp?leaveID=<%= l.getLeaveID()%>",
                                                        success: function (result) {
                                                            $('#comment-section').empty().html(result).hide().fadeIn(300);
                                                            swal({
                                                                title: "Cuti telah " + stringnoti,
                                                                text: "Sila semak e-mail untuk maklumat lanjut",
                                                                type: "success"
                                                            }, function () {
                                                                $(location).attr('href', 'PathController?process=viewleavedetail&leaveID=' + id);
                                                            });
                                                        }
                                                    });


                                                }
                                            });


                                        }, function (ev) {
                                    ev.preventDefault();
                                    //alertify.error("You've clicked Cancel");
                                }
                                );
                    } else {
                        swal({
                            title: "Cuti telah " + stringnoti,
                            text: "Sila semak e-mail untuk maklumat lanjut",
                            type: "success"
                        }, function () {
                            $(location).attr('href', 'PathController?process=viewleavegrid&leaveID=' + id);
                        });

                    }

                }
            });
            //} 

            e.stopPropagation();
            return false;
        });




        $("#back").click(function () {
            parent.history.back();
            return false;
        });

        $(".askdetailfirst").click(function () {


            var id = $(this).attr('id');

            alertify
                    .defaultValue("")
                    .prompt("Nyatakan soalan dan komen anda",
                            function (val, ev) {
                                ev.preventDefault();
                                $.ajax({
                                    async: true,
                                    url: "PathController?process=leavecomment&leaveID=" + id + "&comment=" + val +"&commentID=None",
                                    success: function (result) {
                                        //if (result > 0) {

                                        //}
                                        alertify.success("Mesej anda telah disimpan.");
                                        //$(location).attr('href', 'leave_list_approval.jsp?tab=2');
                                        $.ajax({
                                            url: "leave_comment.jsp?leaveID=<%= l.getLeaveID()%>",
                                            success: function (result) {
                                                $('#comment-section').empty().html(result).hide().fadeIn(300);
                                            }
                                        });


                                    }
                                });


                            }, function (ev) {
                        ev.preventDefault();
                        alertify.error("You've clicked Cancel");
                    }
                    );
            return false;
        });

        $('#comment-section').on('click', '.askdetail', function (e) {
            e.preventDefault();


            var id = $(this).attr('id');
            var commentid = $(this).attr('href');

            alertify
                    .defaultValue("")
                    .prompt("Nyatakan soalan dan komen anda",
                            function (val, ev) {
                                ev.preventDefault();
                                $.ajax({
                                    async: true,
                                    url: "PathController?process=leavecomment&leaveID=" + id + "&comment=" + val + "&commentID="+commentid,
                                    success: function (result) {
                                        //if (result > 0) {

                                        //}
                                        alertify.success("Mesej anda telah disimpan.");
                                        //$(location).attr('href', 'leave_list_approval.jsp?tab=2');
                                        $.ajax({
                                            url: "leave_comment.jsp?leaveID=<%= l.getLeaveID()%>",
                                            success: function (result) {
                                                $('#comment-section').empty().html(result).hide().fadeIn(300);
                                            }
                                        });


                                    }
                                });


                            }, function (ev) {
                        ev.preventDefault();
                        alertify.error("You've clicked Cancel");
                    }
                    );
            return false;
        });

        $('.edit-leave').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $(location).attr('href', 'leave_detail_edit.jsp?id=' + id);

            return false;
        });

        $('.change-leave').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $(location).attr('href', 'leave_detail_edit.jsp?id=' + id);

            return false;
        });

        $('.cancel-leave').click(function (e) {
            var id = $(this).attr('id');
            var tab = $(this).attr('title');
            swal({
                title: "Batalkan cuti ini?",
                text: "Permintaan ini akan disemak terlebih dahulu",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, Batal!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=cancelleave&leaveID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list_approval.jsp?tab=' + tab);
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });


        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            var tab = $(this).attr('title');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list_approval.jsp?tab=' + tab);
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });

        $(".requestto").unbind('click').bind('click', function (e) {

            $(this).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Tunggu ... ');
//var a = $("form").serialize();
            var id = $(this).attr('id');
            var action = $(this).attr('title');

            var stringnoti = 'disahkan';

            if (action == 'Rejected') {
                stringnoti = 'dibatalkan';
            }

            $.ajax({
                async: true,
                url: "PathController?process=validateleave&leaveID=" + id + "&action=" + action,
                success: function (result) {
                    //if (result > 0) {

                    //}


                    swal({
                        title: "Cuti telah " + stringnoti,
                        text: "Sila semak e-mail untuk maklumat lanjut",
                        type: "success"
                    }, function () {
                        $(location).attr('href', 'PathController?process=viewleavedetail&leaveID=' + id);
                    });



                }
            });
            //} 

            e.stopPropagation();
            return false;
        });

        $('body').on('click', '#view-modal-list-leave', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "PathController?process=viewleavelistmodal&leaveID=" + leaveid + "&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle');
            //$(location).attr('href', 'PathController?process=viewleavedetail&leaveID=' + leaveid);
            return false;
        });


    });

</script>

<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>

        <div class="content-wrapper">
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <form class="modal-content" id="savecardestination">
                            <input type="hidden" name="destID" id="destID" value="<%= destID%>">
                            <input type="hidden" name="bookID" id="bookID" value="<%= l.getLeaveID()%>">
                            <div class="modal-header p-4">
                                <a class="text-blue" id="back"><i class="fa fa-chevron-left"></i><span class="button-text"> Kembali</span></a> <h5 class="modal-title">Permohonan Cuti</h5><span class="badge badge-<%= LeaveDAO.getBadgeColor(l.getStatus())%>"><%= LeaveDAO.getMalayWord2(l.getStatus())%></span>

                            </div>
                            <div class="modal-footer justify-content-between bg-primary-50">
                                <div>
                                    <%
                                        if (!LeaveDAO.isButtonDisable(log, l.getLeaveID())) {
                                    %>
                                    <button class="btn btn-success btn-rounded mr-1 actionto" title="<%= titleValid%>" id="<%=l.getLeaveID()%>"><i class="fa fa-check"></i><span class="button-text"> <%= buttonValid%></span></button>
                                    <button class="btn btn-danger btn-rounded mr-3 actionto" title="Rejected" id="<%=l.getLeaveID()%>"><i class="fa fa-remove"></i><span class="button-text"> Tolak</span></button>


                                    <%
                                        }
                                        if (l.getStatus().equals("Preparing")) {

                                    %>
                                    <button class="btn btn-sm btn-warning btn-rounded edit-leave" id="<%= l.getLeaveID()%>"><i class="fa fa-pencil"></i></button>&nbsp;

                                    <button class="btn btn-sm btn-danger btn-rounded delete-leave" id="<%= l.getLeaveID()%>" title="1"><i class="fa fa-trash"></i></button>&nbsp;
                                        <%
                                            }
                                        %>
                                        <%
                                           // if (l.getStatus().equals("Approved") && l.getStaffID().equals(log.getUserID())) {

                                        %>
                                    <!--<button class="btn btn-warning btn-rounded change-leave" id="<%= l.getLeaveID()%>" title="1"><i class="fa fa-retweet" aria-hidden="true"></i><span class="button-text-approve"> Pinda</span></button>-->
                                    <!--<button class="btn btn-danger btn-rounded cancel-leave" id="<%= l.getLeaveID()%>" title="1"><i class="fa fa-ban" aria-hidden="true"></i><span class="button-text-approve"> Batal</span></button>-->
                                            <%
                                              //  }

                                                if (l.getStatus().equals("Approved") && log.getAccessLevel() == 1 && LeaveDAO.isLeaveRequestExist(log, l.getLeaveID(), "cancel")) {

                                            %>
                                    <button class="btn btn-primary btn-rounded mr-3 requestto" title="Rejected" id="<%=l.getLeaveID()%>"><i class="fa fa-remove"></i><span class="button-text"> Tolak</span></button>
                                            <%
                                                }
                                            %>
                                    <!--<label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-edit font-20"></i>
                                        <input type="file">
                                    </label>
                                    <label class="btn btn-sm btn-transparent btn-secondary btn-icon-only btn-circle file-input mb-0"><i class="la la-image font-20"></i>
                                        <input type="file">
                                    </label>-->


                                </div>
                                <button class="btn btn-blue btn-rounded mr-3 askdetailfirst pull-right" title="Ask" id="<%=l.getLeaveID()%>"><i class="fa fa-comment"></i><span class="button-text"> Komen</span></button>

                            </div>
                            <div class="modal-body p-4">
                                <div>
                                    <div>
                                        <%
                                            if (!log.getUserID().equals(st.getStaffid())) {
                                        %>
                                        <!--<h5 class="font-strong mb-4"><%= st.getName()%></h5>-->
                                        <div class="flexbox-b mb-4">
                                            <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="40" />
                                            <div class="flex-1">

                                                <div class="font-strong font-14">&nbsp;&nbsp;<%= st.getName()%><small class="text-muted float-right"><%= l.getDateapply()%></small></div>
                                                <div class="text-muted">&nbsp;&nbsp;<%= st.getPosition()%></div>
                                                <div class="text-muted">&nbsp;&nbsp;<%= st.getLocation()%></div><br>


                                            </div>

                                        </div>
                                        <%
                                            }
                                        %>
                                        <div class="row">

                                            <%
                                                if (l.getType().equals("Cuti Tahunan")) {
                                            %>
                                            <div class="col-6 flexbox-b"><i class="la la-calendar-check-o font-26 text-light mr-3"></i>
                                                <div class="flex-1">
                                                    <div>
                                                        <span class="text-light mr-3">Tahun</span><%= totLeaveUse%> / <%= GeneralTerm.PrecisionDoubleWithoutDecimal(totLeaveYear)%></div>
                                                    <div class="progress mt-1">
                                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentYear)%>" role="progressbar" style="width:<%= percentYear%>%; height:5px;" aria-valuenow="<%= totLeaveUse%>" aria-valuemin="0" aria-valuemax="<%= totLeaveYear%>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 flexbox-b"><i class="la la-hourglass-1 font-26 text-light mr-3"></i>


                                                <div class="flex-1">
                                                    <div>
                                                        <span class="text-light mr-3">Bulan</span><%= totLeaveUseMonth%> / <%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveForTheMonthExcludeApprovedThisMonth(log, st.getStaffid(), yearApply, l.getDatestart()))%></div>
                                                    <div class="progress mt-1">
                                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentMonth)%>" role="progressbar" style="width:<%= percentMonth%>%; height:5px;" aria-valuenow="<%= percentMonth%>" aria-valuemin="0" aria-valuemax="<%= LeaveDAO.getEligibleLeaveForTheMonth(log, st.getStaffid(), yearApply, l.getDatestart())%>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%
                                            } else if (l.getType().equals("Cuti Sakit")) {
                                            %>
                                            <div class="col-12 flexbox-b"><i class="la la-medkit font-26 text-light mr-3"></i>
                                                <div class="flex-1">
                                                    <div>
                                                        <span class="text-light mr-3">Tahun</span><%= totMedicalLeaveUse%> / <%= GeneralTerm.PrecisionDoubleWithoutDecimal(totMedicalLeaveYear)%></div>
                                                    <div class="progress mt-1">
                                                        <div class="progress-bar bg-<%= LeaveDAO.getProgressBarColor(log, percentYearMedical)%>" role="progressbar" style="width:<%= percentYearMedical%>%; height:5px;" aria-valuenow="<%= totMedicalLeaveUse%>" aria-valuemin="0" aria-valuemax="<%= totMedicalLeaveYear%>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                
                                <%
                                if(LeaveDAO.getCountLeaveTodaySummaryByDept(log, l.getDatestart()) > 0){    
                                    %>
                                <span class="ml-3"><small><i class="fa fa-info-circle mr-2" aria-hidden="true"></i>Yang memohon cuti pada hari ini.</small></span>
                                <%}else{%>
                                 <span class="ml-3"><small><i class="fa fa-info-circle mr-2" aria-hidden="true"></i>Tiada permohonan cuti pada hari ini.</small></span>
                                <%}%>
                                <p class="mb-0 ml-3">
                                    <a class="text-blue" href="javascript:;"><%= LeaveDAO.getAllLeaveTodaySummaryString(log, l.getDatestart())%></a>
                                </p>

                                <%
                                    boolean t = LeaveDAO.isNewLeaveExceedEligibleMonth(log, st.getStaffid(), yearApply, l.getDatestart(), l.getDays(), l.getType());

                                    if (!l.getStatus().equalsIgnoreCase("Approved") && !l.getStaffID().equals("P0004")) {
                                        if (t && !l.getStaffID().equals("P0004")) {//ejas sekejap untuk cuti en ihsan
                                %>

                                <div class="alert alert-danger alert-dismissable fade show alert-outline has-icon mt-3"><i class="la la-info-circle alert-icon"></i>

                                    <div class="d-flex align-items-center justify-content-between">
                                        <div><strong>Tidak Layak!</strong><br>Cuti melebihi baki layak.</div>

                                    </div>
                                </div>
                                <%
                                } else {
                                %>
                                <div class="alert alert-success alert-dismissable fade show alert-outline has-icon mt-3"><i class="la la-check alert-icon"></i>

                                    <div class="d-flex align-items-center justify-content-between">
                                        <div><strong>Layak!</strong><br>Cuti tidak melebihi baki.</div>

                                    </div>
                                </div>
                                <%
                                        }
                                    }

                                %>

                                <div class="col-md-12">
                                    <div class="card mb-4">
                                        <div class="rel">
                                            <%                                        if (l.getType().equals("Cuti Sakit")) {
                                            %>
                                            <img class="card-img-top"  src="uploaded_attachment/<%= LeaveDAO.getLeaveAttachment(log, l.getLeaveID()).getFilename()%>" alt="image" />



                                            <div class="card-img-overlay">
                                                <span class="badge badge-default">LAMPIRAN</span>
                                            </div>
                                            <%}%>
                                        </div>
                                        <div>
                                            <hr>
                                            <div class="row">
                                                <%
                                                    if (l.getDays() > 1) {
                                                %>
                                                <div class="col-6">
                                                    <a class="text-muted" href="javascript:;"><small>DARI</small></a>
                                                    <div class="mt-0"><%= AccountingPeriod.fullDateMonth(l.getDatestart())%></div>
                                                </div>
                                                <div class="col-6">
                                                    <a class="text-muted" href="javascript:;"><small>HINGGA</small></a>
                                                    <div class="mt-0"><%= AccountingPeriod.fullDateMonth(l.getDateend())%></div>
                                                </div>
                                                <%} else {%>
                                                <div class="col-6">
                                                    <a class="text-muted" href="javascript:;"><small>PADA</small></a>
                                                    <div class="mt-0"><%= AccountingPeriod.fullDateMonth(l.getDatestart())%></div>
                                                </div>
                                                <%}%>
                                            </div>
                                            <hr>
                                            <div class="row">

                                                <div class="col-6">
                                                    <a class="text-muted" href="javascript:;"><small>JENIS</small></a>
                                                    <div class="mt-0"><%= l.getType()%></div>
                                                </div>
                                                <div class="col-6">
                                                    <a class="text-muted" href="javascript:;"><small>HARI</small></a>
                                                    <div class="mt-0"><%= l.getDays()%></div>
                                                </div>

                                            </div>
                                            <hr>
                                            <a class="text-muted" href="javascript:;"><small>SEBAB</small></a>
                                            <div class="mb-3"><%= l.getReason()%></div>
                                            <hr>
                                            <ul class="timeline">
                                                <li class="timeline-item">
                                                    <span class="timeline-point bg-blue"></span>
                                                    <span> <small>Cuti telah dimohon </small>
                                                    </span>

                                                    <small class="float-right text-muted ml-2 nowrap"><%= AccountingPeriod.fullDateMonth(l.getDateapply()).substring(0, 6)%>, <%= l.getTimeapply().substring(0, 5)%></small>
                                                </li>
                                                <%if (!l.getCheckDate().equals("0000-00-00")) {%>
                                                <li class="timeline-item">
                                                    <span class="timeline-point bg-warning"></span>
                                                    <span> <small>Disemak oleh </small>
                                                        <img class="img-circle ml-0" src="<%= StaffDAO.getInfo(log, l.getCheckID()).getImageURL()%>" alt="image" width="25"  data-toggle="tooltip" data-placement="top" data-trigger="click" title="Clickable Tooltip"/></span>

                                                    <small class="float-right text-muted ml-2 nowrap"><%= AccountingPeriod.fullDateMonth(l.getCheckDate()).substring(0, 6)%>, <%= l.getChecktime().substring(0, 5)%></small>
                                                </li>
                                                <%}
                                                    if (!l.getSupervisorDate().equals("0000-00-00")) {%>  
                                                <li class="timeline-item">
                                                    <span class="timeline-point bg-pink"></span>
                                                    <span><small> Disokong oleh </small>
                                                        <img class="img-circle ml-0" src="<%= StaffDAO.getInfo(log, l.getSupervisorID()).getImageURL()%>" alt="image" width="25" /></span>

                                                    <small class="float-right text-muted ml-2 nowrap"><%= AccountingPeriod.fullDateMonth(l.getSupervisorDate()).substring(0, 6)%>, <%= l.getSpvtime().substring(0, 5)%></small>
                                                </li>
                                                <%}
                                                    if (!l.getHeadDate().equals("0000-00-00")) {%>  
                                                <li class="timeline-item">
                                                    <span class="timeline-point bg-success"></span>
                                                    <span> <small>Dilulus oleh </small>
                                                        <img class="img-circle ml-0" src="<%= StaffDAO.getInfo(log, l.getHeadID()).getImageURL()%>" alt="image" width="25" /></span>

                                                    <small class="float-right text-muted ml-2 nowrap"><%= AccountingPeriod.fullDateMonth(l.getHeadDate()).substring(0, 6)%>, <%= l.getHeadtime().substring(0, 5)%></small>
                                                </li>
                                                <%}%>

                                            </ul>


                                        </div>
                                    </div>
                                </div>




                            </div>

                        </form>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" id="comment-section">

                    </div>
                </div>

            </div>

            <div id="modalhere"></div>
            <div id="modalpassengerdiv"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>


    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>


</body>

<%} else {%>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>

        <div class="content-wrapper">
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-warning alert-dismissable fade show has-icon"><i class="la la-warning alert-icon"></i>
                            <button class="close" data-dismiss="alert" aria-label="Close"></button><strong>Rekod tiada!</strong><br>Kemungkinan cuti tersebut telah dipadam atau tiada di dalam sistem.</div>
                    </div>



                </div>

            </div>
            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>


    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>


</body>
<%}%>


<script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>




</html>

<%
} else {
%>


<script type="text/javascript">
    window.location.href = "index.jsp";
</script>

<%
    }


%>
