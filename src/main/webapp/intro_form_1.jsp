<%-- 
    Document   : intro_form_1
    Created on : Jan 23, 2020, 9:39:37 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String staffID = request.getParameter("staffIDX");

    String name = "";
    String email = "";
    String placeID = "";
    if (staffID.equals("")) {
        //staffID = "Tiada";
        placeID = "Sila masukkan ID pekerja";
        name = request.getParameter("name");
    } else {
        CoStaff st = (CoStaff) StaffDAO.getInfo(log, staffID);
       name = st.getName();
    }


%>
<div class="form-group">
    <label>First Name</label>
    <input class="form-control required" id="name" type="text" name="name" value="<%= name%>">
</div>
<div class="form-group">
    <label>ID Pekerja</label>
    <input class="form-control required" id="staffID" type="text" name="staffID" placeholder ="" value="<%= staffID%>">
</div>
<div class="form-group">
    <label>Email</label>
    <input class="form-control required" id="email" type="text" name="email" value="<%= log.getEmail()%>">
</div>