<%-- 
    Document   : notification
    Created on : Feb 22, 2020, 7:16:53 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.Notify"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.dao.NotifyDAO"%>
<%@page import="com.lcsb.eleave.model.Notification"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    //String yearApply = AccountingPeriod.getCurYearByDate(AccountingPeriod.getCurrentTimeStamp());

    List<Notification> listAllx = (List<Notification>) NotifyDAO.getAllNotificationByStaffID(log);
    int i = 0;
%>

<a class="nav-link dropdown-toggle toolbar-icon" data-toggle="dropdown" href="javascript:;"><i class="ti-bell rel"></i>
     <%
        if(!listAllx.isEmpty()){
        %>
    <span class="envelope-badge"><%= listAllx.size()%></span>
    <%
            }
        %>
        
        
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-menu-media">
    
    <%
        if(listAllx.isEmpty()){
        %>
        <div class="dropdown-arrow"></div>
    <div class="dropdown-header text-center">
        <div>
            <span class="font-18"><strong>Tiada</strong> Notifikasi Baru</span>
        </div>
        <!--<a class="text-muted font-13" href="javascript:;">Sila jenguk lagi</a>-->
    </div>
        <%
            }else{
        %>
    <div class="dropdown-arrow"></div>
    <div class="dropdown-header text-center">
        <div>
            <span class="font-18"><strong><%= listAllx.size()%></strong> Notifikasi Baru</span>
        </div>
       <a class="text-muted font-13" href="javascript:;">Papar Semua</a>
    </div>
    <div class="p-3 ">
        <ul class="timeline scroller" data-height="320px">
            <%

                for (Notification j : listAllx) {
                    i++;

                    Notify n = (Notify) NotifyDAO.getNotify(j);
                    String staffID = LeaveDAO.getLeaveInfoDetail(log, j.getLeaveID()).getStaffID();
                    
                    CoStaff cs = new CoStaff();
                    if(staffID != null){
                        cs = (CoStaff) StaffDAO.getInfo(log, staffID);
                    }
                    
            %>
            <li class="timeline-item"><i class="<%= n.getIcon()%> timeline-icon"></i> <a class="gotonoti" id="<%= j.getLeaveID()%>" href="<%= j.getId() %>"><img class="img-circle" src="<%=cs.getImageURL()%>" alt="image" width="50" />&nbsp;&nbsp;<%= n.getWord()%></a><small class="float-right text-muted ml-2 nowrap"><span class="cd-date need_to_be_rendered" datetime="<%= j.getDate() + " " + j.getTime()%>"></span></small></li>

            <%
                }
            %>
            <!--<li class="timeline-item"><i class="ti-announcement timeline-icon"></i>
                <span>7 new feedback
                    <span class="badge badge-warning badge-pill ml-2">important</span>
                </span><small class="float-right text-muted">5 mins</small></li>
            <li class="timeline-item"><i class="ti-truck timeline-icon"></i>25 new orders sent<small class="float-right text-muted ml-2 nowrap">24 mins</small></li>
            <li class="timeline-item"><i class="ti-shopping-cart timeline-icon"></i>12 New orders<small class="float-right text-muted ml-2 nowrap">45 mins</small></li>
            <li class="timeline-item"><i class="ti-user timeline-icon"></i>18 new users registered<small class="float-right text-muted ml-2 nowrap">1 hrs</small></li>
            <li class="timeline-item"><i class="ti-harddrives timeline-icon"></i>
                <span>Server Error
                    <span class="badge badge-success badge-pill ml-2">resolved</span>
                </span><small class="float-right text-muted">2 hrs</small></li>
            <li class="timeline-item"><i class="ti-info-alt timeline-icon"></i>
                <span>System Warning
                    <a class="text-purple ml-2">Check</a>
                </span><small class="float-right text-muted ml-2 nowrap">12:07</small></li>
            <li class="timeline-item"><i class="fa fa-file-excel-o timeline-icon"></i>The invoice is ready<small class="float-right text-muted ml-2 nowrap">12:30</small></li>
            <li class="timeline-item"><i class="ti-shopping-cart timeline-icon"></i>5 New Orders<small class="float-right text-muted ml-2 nowrap">13:45</small></li>
            <li class="timeline-item"><i class="ti-arrow-circle-up timeline-icon"></i>Production server up<small class="float-right text-muted ml-2 nowrap">1 days ago</small></li>
            <li class="timeline-item"><i class="ti-harddrives timeline-icon"></i>Server overloaded 91%<small class="float-right text-muted ml-2 nowrap">2 days ago</small></li>
            <li class="timeline-item"><i class="ti-info-alt timeline-icon"></i>Server error<small class="float-right text-muted ml-2 nowrap">2 days ago</small></li>-->
        </ul>
    </div>
            
             <%
            }
        %>
</div>




<script src="./assets/js/timeago.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        new timeago().render($('.need_to_be_rendered'));
        
        $('.gotonoti').click(function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var notyid = $(this).attr('href');
            $.ajax({
                async: false,
                url: "ProcessController?process=seennotify&id=" + notyid,
                success: function (result) {
                   $(location).attr('href', 'PathController?process=viewleavedetail&leaveID=' + leaveid);

                }
            });


            
            return false;
        });

    });
</script>