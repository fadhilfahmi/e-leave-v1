<%-- 
    Document   : booking_summary
    Created on : Nov 6, 2019, 4:57:08 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.smartbooking.dao.DateAndTime"%>
<%@page import="com.lcsb.smartbooking.model.BookingDestinationTemp"%>
<%@page import="com.lcsb.smartbooking.model.Members"%>
<%@page import="com.lcsb.smartbooking.dao.MemberDAO"%>
<%@page import="com.lcsb.smartbooking.model.BookingPassengerTemp"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.smartbooking.dao.BookingDAO"%>
<%@page import="com.lcsb.smartbooking.model.BookingMasterTemp"%>
<%@page import="com.lcsb.smartbooking.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    BookingMasterTemp bm = (BookingMasterTemp) BookingDAO.getBookingMasterTemp(log, request.getParameter("sessionid"));
%>
<div class="col-sm-6">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Destination</div>
        </div>
        <div class="ibox-body">

            <div class="cd-timeline cd-timeline-dark  timeline-2">
                <%List<BookingDestinationTemp> listAllDest = (List<BookingDestinationTemp>) bm.getListDestination();

                    for (BookingDestinationTemp j : listAllDest) {

                %>
                <div class="cd-timeline-block">
                    <div class="cd-timeline-icon bg-success text-white"><i class="fa fa-map-marker"></i></div>
                    <div class="cd-timeline-content">
                        <h4><%= j.getDesttype() %></h4>
                        <p><%= j.getDestdescp() %></p>
                        <a class="btn btn-success btn-sm"
                           href="javascript:;">More ...</a>
                        <span class="cd-date"><%= DateAndTime.fullDateMonth(j.getStartdate()) %></span>
                    </div>
                </div>
                <%
                    }
%>
                <!--<div class="cd-timeline-block">
                    <div class="cd-timeline-icon bg-primary text-white"><i class="fa fa-briefcase"></i></div>
                    <div class="cd-timeline-content">
                        <h4>Meeting</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                        <a class="btn btn-success btn-sm"
                           href="javascript:;">More ...</a>
                        <span class="cd-date">2 Days ago</span>
                    </div>
                </div>
                <div class="cd-timeline-block">
                    <div class="cd-timeline-icon bg-warning text-white"><i class="fa fa-shopping-basket"></i></div>
                    <div class="cd-timeline-content">
                        <h4>Shopping with Olivia</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                        <a class="btn btn-success btn-sm"
                           href="javascript:;">More ...</a>
                        <span class="cd-date">Dec 15</span>
                    </div>
                </div>
                <div class="cd-timeline-block">
                    <div class="cd-timeline-icon bg-blue text-white"><i class="fa fa-birthday-cake"></i></div>
                    <div class="cd-timeline-content">
                        <h4>Emma's birthday</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                        <a class="btn btn-success btn-sm"
                           href="javascript:;">More ...</a>
                        <span class="cd-date">Dec 17</span>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="ibox ibox-fullheight">
        <div class="ibox-head">
            <div class="ibox-title">Passenger List</div>
        </div>
        <div class="ibox-body">
            <ul class="media-list media-list-divider mr-2 scroller" data-height="480px">
                <%List<BookingPassengerTemp> listAll = (List<BookingPassengerTemp>) bm.getListPassenger();

                    for (BookingPassengerTemp j : listAll) {
                        Members m = (Members) MemberDAO.getMemberInfo(log, j.getCompID());

                %>
                <li class="media align-items-center">
                    <a class="media-img" href="javascript:;">
                        <img class="img-circle" src="<%= m.getImageURL() %>" alt="image" width="54" />
                    </a>
                    <div class="media-body d-flex align-items-center">
                        <div class="flex-1">
                            <div class="media-heading"><%= m.getMemberName()%></div><small class="text-muted"><%= m.getEmail()%></small></div>
                        <!--<button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>-->
                    </div>
                </li>
                <%                                        }
                %>

            </ul>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("[name='timeline1-option']").change(function () {
            +this.value ? $('.timeline-1').addClass('center-orientation') : $('.timeline-1').removeClass('center-orientation');
        });
        $("[name='timeline2-option']").change(function () {
            +this.value ? $('.timeline-2').addClass('center-orientation') : $('.timeline-2').removeClass('center-orientation');
        });
    })
</script>