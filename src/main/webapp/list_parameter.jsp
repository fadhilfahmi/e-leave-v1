<%-- 
    Document   : list_parameter
    Created on : Mar 2, 2016, 8:57:27 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>

<%
    if (request.getParameter("keyword") == null) {

%>    
<div class="list-group"><% List<Parameter> slist = (List<Parameter>) ParameterDAO.getAllParameter(log, request.getParameter("type"));
    for (Parameter c : slist) {
    %>

    <a href="<%= c.getParameter()%>" id="<%= c.getValue()%>" class="list-group-item thisresult">
        <div id="right_div"><%= c.getValue()%></div>
    </a>

    <%
        }
    %>
</div>

<%
} else {
%>    
<div class="list-group"><%
    List<Parameter> alist = (List<Parameter>) ParameterDAO.searchParameter(log, request.getParameter("type"), request.getParameter("keyword"));
    for (Parameter c : alist) {
    %>

    <a href="<%= c.getParameter()%>" id="<%= c.getValue()%>" class="list-group-item thisresult">
        <div id="right_div"><%= c.getValue()%></div>
    </a>

    <%
        }
    %>
</div>

<%
    }
%> 